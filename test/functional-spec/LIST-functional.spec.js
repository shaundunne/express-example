var config = require('../../src/config.js');
var request = require('request');
var validArticleId = '532883156414100b1ab1fc12';
var global_config = require('../../../config/global.js');
var listUrl = global_config.settings.server.WEB;

describe('Given that LAYOUT is up and running,', function () {
    it("should respond to 'get /' with status code 200", function(done) {
        request(listUrl, function(error, response, body){
            expect(response.statusCode).toEqual(200);
            done();
        });
    });

    it("should respond to 'get /' with html", function(done) {
        request(listUrl, function(error, response, body){
            expect(body).toContain('<html>');
            done();
        });
    });

    it("should serve the responsive layout at '/?c=false'" ,function(done){
        request(listUrl + '?c=false', function(error, response, body){
            expect(body).toContain('class="responsive"');
            done();
        });
    });

    it("should serve the responsive layout at '/?js=false'" ,function(done){
        request(listUrl + '?c=false', function(error, response, body){
            expect(body).toContain('class="responsive');
            done();
        });
    });

    it("should serve the cookie checker at '/' if I have no cookie set" ,function(done){
        request(listUrl, function(error, response, body){
            expect(body).toContain('Browser feature check');
            done();
        });
    });

    it("should serve the responsive site if my cookie has viewport_width = 800" ,function(done){
        // create our own cookie jar for this test
        var j = request.jar();
        // set the viewport_width cookie to 800
        var cookie = request.cookie('viewport_width=800');
        j.setCookie(cookie, listUrl);
        request({url: listUrl, jar: j}, function(error, response, body){
            expect(body).toContain('class="responsive');
            done();
        });
    });

    it("should serve the mobile site if my cookie has viewport_width = 320" ,function(done){
        // create our own cookie jar for this test
        var j = request.jar();
        // set the viewport_width cookie to 320
        var cookie = request.cookie('viewport_width=320');
        j.setCookie(cookie, listUrl);
        request({url: listUrl, jar: j}, function(error, response, body){
            expect(body).toContain('class="mobile');
            done();
        });
    });


});

describe('Given that the ARTICLE server is up and running', function(){
    it('should respond with error if invalid article id is requested', function(done) {
        request(listUrl + 'article/123',function(error, response, body){
            expect(response.statusCode).toBeGreaterThan(399);
            done();
        });
    });

    it('should respond status 200 if a valid article id is requested', function(done) {
        request(listUrl + 'article/' + validArticleId,function(error, response, body){
            expect(response.statusCode).toEqual(200);
            done();
        });
    });

    it('should respond with an article if a valid article id is requested', function(done) {
        request(listUrl + 'article/' + validArticleId,function(error, response, body){
            expect(body).toContain('<article>');
            done();
        });
    });
});

describe('Given that the IMAGE server is up and running', function(){
    it('should respond with error if invalid image id is requested', function(done) {
        request(listUrl + 'img/123',function(error, response, body){
            console.log(response.statusCode);
            expect(response.statusCode).toBeGreaterThan(399);
            done();
        });
    });

});

