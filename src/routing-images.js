var imageHandler = require('./image-handler');
var config = require('./config');
var db = require('./db-interface');

var imageRouting = function (app){

    // Requests for image of specific dimension
    app.get('/img/:filename?/:width/:height?', function (req, listResp) {
        imageHandler.process({ res : listResp, filename : req.params.filename, width: req.params.width, height: req.params.height});
    });


    return app;

};

module.exports = imageRouting;