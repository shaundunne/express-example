// gamification.js

// Load dependencies
var $ = require('jquery');
var event = require('./event');
var emitter = event.emitter;
var activityFeedPublisher = require('./activity-feed-publisher');



var callback = function(){
	gigya.gm.showNotifications();
};

var registerUpvotePoints = function( ev ){

	var params = {
		action: "generalUpvote",  // this action ID should be defined in the Action Setup page in Gigya's site.
		callback : callback
	};
	
	activityFeedPublisher.publish({
		headline : ev.headline,
		interaction : 'Upvoted',
		url : ev.url
	});

	if ( ev.category ) {
		var categoryAction = ev.category + "Upvote";
		gigya.gm.notifyAction( { action : categoryAction, callback : callback } );
	}

	gigya.gm.notifyAction(params);

};

var registerRevokeVotePoints = function( ev ){

	if ( ev.category ) {
		var categoryAction = ev.category + "RevokeVote";
		gigya.gm.notifyAction( { action : categoryAction, callback : callback } );
	}

	// this action ID should be defined in the Action Setup page in Gigya's site.
	gigya.gm.notifyAction( { action : 'generalRevokeVote' } );

};

// registerShareAction - called after sharing
var registerShareAction = function( e ){

	activityFeedPublisher.publish({
		headline : e.headline,
		interaction : 'Shared',
		url : e.url
	});

	// Register share via category
	if ( e.category ) {
		var categoryAction = e.category + "Sharing";
		gigya.gm.notifyAction( { action : categoryAction } );
	}

	gigya.gm.notifyAction( { action : 'generalShare' } );
};


var init = function(){
	// register listeners
	emitter.on('beforeVote', registerUpvotePoints);
	emitter.on('beforeRevokeVote', registerRevokeVotePoints);
	emitter.on('share', registerShareAction);
};


module.exports = {
	init : init
};

