/**
 * Comment controller
 *
 * This is a constructor for a comment object which is a child of the article object
 *
 */

var $ = require('jquery');
var events = require('./event');
var emitter = events.emitter;
var social = require('./social.js');
var utils = require('./utils');
var activityFeedPublisher = require('./activity-feed-publisher');

var initCommentModule = function(id, streamID, showComments, onUILoad, postedComment){

	// Define templates to change graphic layout in the Comments Plugin
	var templates= {};

	// Override the header - customised to remove sorting & RSS feeds...
	templates.header = '<div class="gig-comments-header-left"><div class="gig-comments-count"></div></div>';
	// default:       '<div class="gig-comments-header-left"><div class="gig-comments-count"></div></div><ul class="gig-comments-header-right gig-comments-linksContainer"><li class="gig-comments-sort">$sort</li><li class="gig-comments-subscribe">$subscribe</li><li class="gig-comments-rss">$rss</li></ul>';


	// Comment template - customised to remove the share option...
	templates.comment = '<div class="gig-comment-title"></div><div class="gig-comment-photo"></div><div class="gig-comment-data"><div class="gig-comment-self-data"><div class="gig-comment-content"><div class="gig-comment-header"><div class="gig-comment-header-left"><span class="gig-comment-username"></span></div><div class="gig-comment-header-right"><div class="gig-comment-viaProvider"></div><span class="gig-comment-time"></span></div></div><div class="gig-comment-status"></div><div class="gig-comment-body"></div><div class="gig-comment-mediaItem"></div></div><div class="gig-comment-footer"><div class="gig-comment-footer-left"><ul class="gig-comments-linksContainer"><li class="gig-comment-likeLink">$like</li><li class="gig-comment-replyLink">$reply</li><li class="gig-comment-deleteLink">$delete</li><li class="gig-comment-repliesArrow"><div class="gig-comment-repliesArrow-text"></div><div class="gig-comment-repliesArrow-img"></div></li></ul><div class="gig-comment-flag"></div></div><div class="gig-comment-footer-right"><div class="gig-comment-vote-total"></div><div class="gig-comment-vote-pos"></div><div class="gig-comment-vote-neg"></div></div></div></div><div class="gig-comment-replybox"></div><div class="gig-comment-replies"></div></div>';
	// default :       '<div class="gig-comment-title"></div><div class="gig-comment-photo"></div><div class="gig-comment-data"><div class="gig-comment-self-data"><div class="gig-comment-content"><div class="gig-comment-header"><div class="gig-comment-header-left"><span class="gig-comment-username"></span></div><div class="gig-comment-header-right"><div class="gig-comment-viaProvider"></div><span class="gig-comment-time"></span></div></div><div class="gig-comment-status"></div><div class="gig-comment-body"></div><div class="gig-comment-mediaItem"></div></div><div class="gig-comment-footer"><div class="gig-comment-footer-left"><ul class="gig-comments-linksContainer"><li class="gig-comment-likeLink">$like</li><li class="gig-comment-replyLink">$reply</li><li class="gig-comment-deleteLink">$delete</li><li class="gig-comment-shareLink">$share</li><li class="gig-comment-repliesArrow"><div class="gig-comment-repliesArrow-text"></div><div class="gig-comment-repliesArrow-img"></div></li></ul><div class="gig-comment-flag"></div></div><div class="gig-comment-footer-right"><div class="gig-comment-vote-total"></div><div class="gig-comment-vote-pos"></div><div class="gig-comment-vote-neg"></div></div></div></div><div class="gig-comment-replybox"></div><div class="gig-comment-replies"></div></div>'

	// Composebox template - customised to remove sharing
	templates.composebox = '<div class="gig-composebox-error"></div><div class="gig-composebox-header"><div class="gig-composebox-login"><div class="gig-composebox-social-login">$login<div class="gig-composebox-login-icon"></div><div class="gig-composebox-login-drop-icon"></div></div><div class="gig-composebox-site-login">$login</div><div class="gig-composebox-or">$or</div><div class="gig-composebox-guest-login">$guest</div></div><div class="gig-composebox-header-right"><div class="gig-composebox-follow"></div><div class="gig-composebox-close"></div></div><div class="gig-composebox-header-left"><div class="gig-composebox-title"></div><div class="gig-composebox-logout">(<span>$logout</span>)</div><div class="gig-composebox-ratings"></div></div></div><div class="gig-composebox-photo"></div><div class="gig-composebox-data"><div class="gig-composebox-summary"><input class="gig-composebox-summary-input" data-placeholder="$summary" /></div><div class="gig-composebox-editor"></div><div class="gig-composebox-mediaItem"></div><div class="gig-composebox-footer"><div class="gig-composebox-footer-right"><div class="gig-composebox-post">$post</div></div></div></div>';
	// default :          '<div class="gig-composebox-error"></div><div class="gig-composebox-header"><div class="gig-composebox-login"><div class="gig-composebox-social-login">$login<div class="gig-composebox-login-icon"></div><div class="gig-composebox-login-drop-icon"></div></div><div class="gig-composebox-site-login">$login</div><div class="gig-composebox-or">$or</div><div class="gig-composebox-guest-login">$guest</div></div><div class="gig-composebox-header-right"><div class="gig-composebox-follow"></div><div class="gig-composebox-close"></div></div><div class="gig-composebox-header-left"><div class="gig-composebox-title"></div><div class="gig-composebox-logout">(<span>$logout</span>)</div><div class="gig-composebox-ratings"></div></div></div><div class="gig-composebox-photo"></div><div class="gig-composebox-data"><div class="gig-composebox-summary"><input class="gig-composebox-summary-input" data-placeholder="$summary" /></div><div class="gig-composebox-editor"></div><div class="gig-composebox-mediaItem"></div><div class="gig-composebox-footer"><div class="gig-composebox-footer-right"><div class="gig-composebox-post">$post</div></div><div class="gig-composebox-footer-left"><div class="gig-composebox-share"><div class="gig-composebox-share-text">$share</div><div class="gig-composebox-share-providers"></div></div></div></div></div>',


	// Editor template -
	// This template changes the editable div to a text area control which works on an iPad, but it has an issue that
	// you can't post a comment as you get the message please fill in required fields

	//	templates.editor = '<ul class="gig-composebox-sidebar"><li class="gig-composebox-sidebar-button gig-composebox-sidebar-font"></li><li class="gig-composebox-sidebar-button gig-composebox-sidebar-media"></li><li class="gig-composebox-sidebar-button gig-composebox-sidebar-tag"></li></ul><textarea class="gig-composebox-textarea" contenteditable="true" placeholder="Join the conversation"></textarea><p></p>',mediaItemPlayer:'<div class="gig-comments-mediaplayer-top"><div class="gig-media-cancel"></div></div><div class="gig-comments-mediaplayer-content" style="height:${playerHeight}px;">$playerHTML</div><div class="gig-comments-mediaplayer-bottom"><div class="gig-comments-mediaplayer-caption" style="background-image:url($providerIcon);">$caption</div></div>'



// Here are the default templates from Gigya's source code...



//	defaultTemplates={commentsPlugin:'<div class="gig-comments-composebox"></div><div class="gig-comments-header"></div><div class="gig-comments-updates"></div><div class="gig-comments-comments"></div><div class="gig-comments-more"></div>',commentsPluginComposeBottom:'<div class="gig-comments-header"></div><div class="gig-comments-updates"></div><div class="gig-comments-comments"></div><div class="gig-comments-more"></div><div class="gig-comments-composebox"></div>',header:'<div class="gig-comments-header-left"><div class="gig-comments-count"></div></div><ul class="gig-comments-header-right gig-comments-linksContainer"><li class="gig-comments-sort">$sort</li><li class="gig-comments-subscribe">$subscribe</li><li class="gig-comments-rss">$rss</li></ul>',
//		updates:'<div class="gig-comments-updates-text"></div><div class="gig-comments-updates-link"></div>',comment:'<div class="gig-comment-title"></div><div class="gig-comment-photo"></div><div class="gig-comment-data"><div class="gig-comment-self-data"><div class="gig-comment-content"><div class="gig-comment-header"><div class="gig-comment-header-left"><span class="gig-comment-username"></span></div><div class="gig-comment-header-right"><div class="gig-comment-viaProvider"></div><span class="gig-comment-time"></span></div></div><div class="gig-comment-status"></div><div class="gig-comment-body"></div><div class="gig-comment-mediaItem"></div></div><div class="gig-comment-footer"><div class="gig-comment-footer-left"><ul class="gig-comments-linksContainer"><li class="gig-comment-likeLink">$like</li><li class="gig-comment-replyLink">$reply</li><li class="gig-comment-deleteLink">$delete</li><li class="gig-comment-shareLink">$share</li><li class="gig-comment-repliesArrow"><div class="gig-comment-repliesArrow-text"></div><div class="gig-comment-repliesArrow-img"></div></li></ul><div class="gig-comment-flag"></div></div><div class="gig-comment-footer-right"><div class="gig-comment-vote-total"></div><div class="gig-comment-vote-pos"></div><div class="gig-comment-vote-neg"></div></div></div></div><div class="gig-comment-replybox"></div><div class="gig-comment-replies"></div></div>',
//		mediaItem:'<div class="gig-media-display"><div class="gig-media-thumbnail"></div><div class="gig-media-overlay"><div class="gig-media-cancel"></div><div class="gig-media-play"></div></div></div><div class="gig-media-caption" > <a href="$url" target="_blank" class="gig-media-caption-url" style="background-image:url($providerIcon);">$displayUrl</a><div class="gig-media-caption-text">$caption</div></div>',dialog:'<div class="gig-comments-dialog"><div class="gig-comments-dialog-caption-container"><div class="gig-comments-dialog-caption"></div><div class="gig-comments-dialog-close"></div></div><div class="gig-comments-dialog-body"></div><div class="gig-comments-dialog-buttons"><div class="gig-comments-dialog-button gig-comments-dialog-button-ok"></div><div class="gig-comments-dialog-button gig-comments-dialog-button-cancel"></div></div></div>',
//		emailTextboxDialogBody:'<div class="gig-comments-dialog-text">$text</div><div class="gig-comments-dialog-textbox-container"><input type="textbox" class="gig-comments-dialog-textbox" value="$email" /></div><div class="gig-comments-dialog-button gig-comments-dialog-textbox-button">$buttonText</div><div class="gig-comments-dialog-error"></div>',photo:'<img class="gig-comment-img" alt="" src="$photoURL" style="vertical-align:top" />$providerImage',providerImage:'<div class="gig-comment-providerLogo"><img alt="" src="$providerImageUrl" style="vertical-align:top" /></div>',
//		composebox:'<div class="gig-composebox-error"></div><div class="gig-composebox-header"><div class="gig-composebox-login"><div class="gig-composebox-social-login">$login<div class="gig-composebox-login-icon"></div><div class="gig-composebox-login-drop-icon"></div></div><div class="gig-composebox-site-login">$login</div><div class="gig-composebox-or">$or</div><div class="gig-composebox-guest-login">$guest</div></div><div class="gig-composebox-header-right"><div class="gig-composebox-follow"></div><div class="gig-composebox-close"></div></div><div class="gig-composebox-header-left"><div class="gig-composebox-title"></div><div class="gig-composebox-logout">(<span>$logout</span>)</div><div class="gig-composebox-ratings"></div></div></div><div class="gig-composebox-photo"></div><div class="gig-composebox-data"><div class="gig-composebox-summary"><input class="gig-composebox-summary-input" data-placeholder="$summary" /></div><div class="gig-composebox-editor"></div><div class="gig-composebox-mediaItem"></div><div class="gig-composebox-footer"><div class="gig-composebox-footer-right"><div class="gig-composebox-post">$post</div></div><div class="gig-composebox-footer-left"><div class="gig-composebox-share"><div class="gig-composebox-share-text">$share</div><div class="gig-composebox-share-providers"></div></div></div></div></div>',
//		editor:'<ul class="gig-composebox-sidebar"><li class="gig-composebox-sidebar-button gig-composebox-sidebar-font"></li><li class="gig-composebox-sidebar-button gig-composebox-sidebar-media"></li><li class="gig-composebox-sidebar-button gig-composebox-sidebar-tag"></li></ul><div class="gig-composebox-textarea" contenteditable="true" data-placeholder="$write_a_comment"></div><p></p>',mediaItemPlayer:'<div class="gig-comments-mediaplayer-top"><div class="gig-media-cancel"></div></div><div class="gig-comments-mediaplayer-content" style="height:${playerHeight}px;">$playerHTML</div><div class="gig-comments-mediaplayer-bottom"><div class="gig-comments-mediaplayer-caption" style="background-image:url($providerIcon);">$caption</div></div>',
//		guestbox:'<div class="gig-guestbox-fields"><input type="text" class="gig-guestbox-nickname" data-placeholder="$nickname" /><input type="text" class="gig-guestbox-email" data-placeholder="$email" /></div><div class="gig-guestbox-text">${email_not_displayed_publicly}</div><div class="gig-guestbox-ok">$ok</div><div class="gig-guestbox-error"></div>',sortbox:'<ul><li data-sort="dateDesc">$most_recent</li><li data-sort="dateAsc">$oldest</li><li data-sort="votesDesc">$most_voted</li><li data-sort="ratingDesc">$ratings</li></ul>',
//		fontbox:'<ul><li class="gig-composebox-font-bold" data-command="bold"></li><li class="gig-composebox-font-italic" data-command="italic"></li><li class="gig-composebox-font-underline" data-command="underline"></li><li class="gig-composebox-font-bullets" data-command="bullets"></li></ul>',mediabox:'<div class="gig-mediabox-fields"><input type="text" class="gig-mediabox-url" data-placeholder="${enter_url}" value="$url" /><div class="gig-mediabox-ok">$ok</div><div class="gig-mediabox-text">${add_media_item}</div></div>',
//		composeShareMoreBox:'<div class="gig-composebox-morebox-caption"><div class="gig-composebox-morebox-title">$more_share_destinations</div><div class="gig-composebox-morebox-close"></div></div><ul class="gig-composebox-morebox-providers"></ul>',userTaggingBox:'<div class="gig-comments-usertaggingbox gig-comments-container"><div class="gig-usertagging-title">$title</div><ul class="gig-usertagging-suggestions"></ul > </div> ',userTaggingOption:'<img src="$photoURL" alt="" class="gig-usertagging-userimage"/><span class="gig-usertagging-username">$name</span>',
//		rating:'<div class="gig-comment-rating-title"></div><div class="gig-comment-rating-value"></div>',composeboxRating:'<div class="gig-composebox-rating-title"></div><div class="gig-composebox-rating-value"></div>',commentTitle:'<div class="gig-comment-rating"></div><div class="gig-comment-rating-drop"></div><div class="gig-comment-summary"></div>',myReview:'<div class="gig-selfreview-header">;<div class="gig-selfreview-header-left">;<div class="gig-composebox-photo"></div>;<div class="gig-selfreview-nameAndLogout">;<div class="gig-selfreview-yourReview">$your_review</div>;<div class="gig-composebox-name"></div>;<div class="gig-composebox-logout">(<span>$logout</span>)</div>;</div>;<div class="gig-selfreview-ratings"></div>;</div>;<div class="gig-selfreview-header-right">;<div class="gig-composebox-follow"></div>;</div>;</div>;<div class="gig-selfreview-summary-container">;<span class="gig-selfreview-field-title">$summary</span><span class="gig-selfreview-summary"></span>;</div>;<div class="gig-selfreview-body-container">;<span class="gig-selfreview-field-title">$review</span><span class="gig-selfreview-body"></span>;</div>'.split(";")}}
//
//
//


	var params =
	{
		// Required parameters:
		categoryID:'article', // this is set up in the GIGYA administration panel
		containerID: id,  // this uniquely defines this stream

		// Optional parameters:
		templates: templates,
		streamID: streamID,
		deviceType: 'auto',
		width: 'auto',
		version: 2,
		cid: '',
		onCommentSubmitted : postedComment,
		onLoad : onUILoad
	}
	// Load the Comments Plugin
	gigya.comments.showCommentsUI(params);
};

// our constructor...
var Comment = function($article, _id){

	// globals
	var $commentContainer,
			id,
			commentID;

	id = _id;
	commentID = 'comment-' + id;


	// comment revealer...
	var showComments = function(){


		$commentContainer.removeClass('hide-comments');

		events.emitter.emit('articleResized',{
			id : id
		})
	};

	var postedComment = function () {
		var $parentArticle = $commentContainer.closest( '[data-id]' );
		var	category = $parentArticle.data('category');

		if ( category ) {
			var categoryAction = category + "Commenting";
			gigya.gm.notifyAction( { action : categoryAction } );
		}

	};


	var postClicked = function(e){
		e.preventDefault();

		publishToActivityFeed( e );

		if (social.isLoggedIn()){
			showComments();
		}
		else{
			e.preventDefault();
			events.emitter.emit('person');
		}
	};

	var publishToActivityFeed = function( e ){
		var $configElem = $(e.target).closest('[data-id]');

		activityFeedPublisher.publish({
			headline : $configElem.data('headline'),
			interaction : 'Commented',
			url : $configElem.data('url')
		});
	};

// onUILoad - triggered after the Gigya UI has loaded
	var onUILoad = function(){
		// find the gigya comments counter and use it to make our 'show n comments' button

		var $commentCount = $commentContainer.find('.gig-comments-count');
		var countText = $commentCount.text();
		
		if(countText !== '0 Comments'){
			$showCommentButton = $('<div class=comment-button>Show ' + countText + '</div>');
			// view comment when clicking view comment...
			$commentContainer
					.on('click', '.comment-button', showComments)
					.prepend($showCommentButton);
		}

	};


	$commentContainer = $article.find('.comment-container');

	// initially hide the comments
	$commentContainer.addClass('hide-comments');


	$commentViewer = $('<div id=' + commentID + ' class=comment-viewer></div>');
	$commentContainer.append($commentViewer);

	// load the gigya plugin...
	initCommentModule(commentID, id, showComments, onUILoad, postedComment);


	// view comments on posting...
	$commentViewer.on('click', '.gig-composebox-post', postClicked);

	// fix issue 7657 - invoke keyboard on iPad
	$commentViewer.on('click', '.gig-composebox-textarea', function(e){
		$(e.target).focus();
	});
};



module.exports = Comment;