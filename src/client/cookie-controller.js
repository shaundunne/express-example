/**
 * Cookie controller
 *
 * This library sets a cookie to the viewport width, and updates it on resize
 */

'use strict';
function updateCookie(){
    var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
    var height = (window.innerHeight > 0) ? window.innerHeight : screen.height;
	var cookie = 'viewport_width=' + Math.min(width, height) + '; path=/;';
    document.cookie = cookie;
};

if(navigator.cookieEnabled){
    updateCookie();
}

window.onresize = function(){
    try{
        document.documentElement.classList.add('resized');
        updateCookie();
    }
    catch (e){}
};