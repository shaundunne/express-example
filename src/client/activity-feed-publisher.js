/* activity-feed-publisher.js
This stuff is mainly from here:
* http://developers.gigya.com/020_Client_API/010_Socialize/socialize.publishUserAction
It's the set up to pull share and vote interaction into the activity feed. 
*/

var activityFeedPublisher = function( params ) {
	
	var headline = params.headline,
		url = params.url,
		interaction = params.interaction,
		userActivity = new gigya.socialize.UserAction(),
		activityParams = {};

	userActivity.setTitle( interaction + ' - ' + headline );		// Setting the Title
	userActivity.setLinkBack( url );						// Setting the Link Back

	activityParams = {
		userAction : userActivity,
		scope : 'internal',
		privacy : 'public'
	};

	// Publishing the User Action			
	gigya.socialize.publishUserAction( activityParams );

};

module.exports = {
	publish : activityFeedPublisher
};