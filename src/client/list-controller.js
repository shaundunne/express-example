/**
 * List controller

 * This is written in the style of NPM for use with browserify
 */

"use strict";
var $ = require('jquery');
var event = require('./event');
var social = require('./social');
var Card = require('./card-controller');
var config = require('./config');
var cards; // temporary object of current cards
var ids = []; // array of known ids - has implicit order of cards :)

var articleManager = require('./articles-manager');

var $listContainer;

var init = function () {

	$listContainer = $('#list-container');

	updateList( $listContainer );

	// Add listeners for actions on the card...
	$listContainer.on('click', '.card .contents', function (e) {
		e.stopPropagation();
		e.preventDefault();

		var card = $(this).parent('.card')[0].card;
		event.emitter.emit('viewArticle', card);
	});


// ... and on every new list load
	event.emitter.on('listLoad', refreshCards);

// on login, poll gigya and see which cards are already voted on.
	event.emitter.on('userLoggedIn', refreshCards);

// Update list when the refresh button is clicked
	event.emitter.on('refreshListRequest', updateList );
// Reset the list if any of the discovery tabs in the main nav get clicked
	event.emitter.on('discoveryLoaded', updateList )


};


var updateList = function( elem ){

	$listContainer = $('#list-container');

	var $drawerOpenFlag = $listContainer.closest( '.drawer-open' );

	// If the drawer is open then shut the drawer, 
	// this is for mobile so that the drawer gets moved away to reveal the discover section
	if ( $drawerOpenFlag ) {
		$drawerOpenFlag.removeClass( 'drawer-open' );
	}

	$.get(config.listUrl + '/list', function (data) {
		// Populate list with new list items
		$listContainer.html(data);
		
		// Let everyone know the list has loaded
		event.emitter.emit('listLoad');

		var primedArticle = $('article').first();

		if(primedArticle[0]){
			// is an article
			event.emitter.emit('scrollToArticle', { id: primedArticle.data('id')} );
		} else {
			event.emitter.emit('scrollToArticle', { id : 0 } );
		}
	});

};



// iterate through the cards in the dom, and add JS card object representation
var refreshCards = function(){
	cards = {};
	ids = [];
	$('.card').each(function(i, v){
		if(!this.card){
			// augment this card dom object with a JS representation
			// the cool thing is that deleting the dom object will delete this object and
			// therefore remove the listeners :)
			this.card = new Card($(this));
		};
		// push ids in order of 100
		ids.push(this.card.id);
		cards[this.card.id] = this.card;
	});

	// send event for article-manager to listen to when this operation is complete
	event.emitter.emit('refreshedCards');
};

// Get a card by ID or one of it's neighbours by specifying an offset
var getCard = function (id, offset) {
	offset = offset || 0;
	var pos = ids.indexOf(id);
	if(pos < 0) {
		return false; //because it means the passed id is not in the list
	}
	var cid = ids[pos + offset];
	return cards[cid];
};

// get card at position 1 - 100
var getCardAt = function(position){
	var id = ids[position - 1];
	return cards[id];
};


// refresh the cards on init
module.exports = {
	init : init,
	updateList : updateList,
	getCard : getCard,
	getCardAt : getCardAt
};

