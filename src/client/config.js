// Get the parameters passed to this library via the script tag
// thanks to...
// http://feather.elektrum.org/book/src.html
var scripts = document.getElementsByTagName('script');
var index = scripts.length - 1;
var myScript = scripts[index];
var config = JSON.parse(myScript.innerHTML);

// Set up other config here...

// scrollArticleUnloadFactor is the number of screen heights an off screen article
// will survive prior to being unloaded from the dom.  Also determines when it should
// be reloaded.
config.scrollArticleUnloadFactor = 0.5;  // measured in fractions of screen height
config.scrollGranularity = 500; // how often poll the scroller in ms

module.exports = config;