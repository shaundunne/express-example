/**
 * Card controller
 *
 * This represents a card in the list.  It is responsible for listening to
 * events that affect its own status, and updating itself accordingly

* */

"use strict";
var $ = require('jquery');
var event = require('./event');
var voteTracker = require('./vote-tracker');

// Card representation.  Listens for relevant events and dispatches appropriate actions
var Card = function($card){
	// Scrape the initial vote count from the DOM
	var $upvote = $card.find('.upvote');
	var $voteCounter = $upvote.find('span');
	var votes = parseInt($voteCounter.text());

	// Scrape the rank from the DOM
	var $rank = $card.find('.rank');
	var rank = parseInt($rank.text());

	// set the id of the card
	var id = $card.data('id');

	// set the displayUrl
	var displayUrl = $card.data('url');
	var headline = $card.find('.headline-inner').text();


	// setVoteCount: set the vote counter to specified value
	var setVoteCount = function(_votes){
		votes = _votes;
		// update the visual representation...
		$voteCounter.html(votes);
	};

	// incrementVoteCount: increment the votecount by  1
	var adjustVoteCount = function(value){
		// set the new vote count
		setVoteCount(votes + value);
		toggleMarkVoted( value === 1 );
	};


	var getVoteCount = function(){
		return votes;
	};

	var getRank = function(){
		return rank;
	};

	// function to update dom representation of voted cards
	var toggleMarkVoted = function(voted){
		$upvote.toggleClass('disabled', voted);
	};

	// mark as voted if this has been previously voted apon
	if(voteTracker.hasVoted(id)){
		toggleMarkVoted(true);
	};

	var upvote = function(){
		adjustVoteCount(1);
	};

	var revokeVote = function(){
		adjustVoteCount(-1);
	};


	return {
		id: id,
		getRank : getRank,
		getVoteCount : getVoteCount,
		upvote : upvote,
		revokeVote : revokeVote,
		$card : $card,
		displayUrl : displayUrl,
		headline : headline
	}
};


module.exports = Card;