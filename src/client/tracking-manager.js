// Tracking manager.

// We'll pinch the server side tracking library for transforming an article into
// a tracking object

var tracking = require('../tracking');
var event = require('./event');
var emitter = event.emitter;

// trackArticle
// Ensure every property we set on the 's' object is replaced
// by the new value for the selected article.

// ref: http://stackoverflow.com/questions/8169265/omniture-tracking-and-jquery
var trackArticle = function(eventObject){
	if (!window.s) {
		console.warn('Omniture is not loaded');
		return;
	}

	// tracker is defined on page, so we need to defensively cover the situation where it fail for some reason
	if(!tracker[eventObject.id]) {
		// create a new tracking object with whatever we know
		console.log('No tracker object for this', eventObject);
		tracker[eventObject.id] = tracking(eventObject);
	}

	for(prop in tracker[eventObject.id]){
		s[prop] = tracker[eventObject.id][prop];
		//console.log('S PROP', s[prop]);
	}

	// fire the Omniture tracking...
	s.t();

};

var init = function(){
	emitter
			.on('articleFocus', function (ob) {
				//console.log('articleFocus', ob);
				trackArticle(ob);
			})
			.on('discoveryLoaded', function (ob) {
				//console.log('discoveryLoaded', ob);
				trackArticle(ob);
			});

};


module.exports = {
	init : init
}


