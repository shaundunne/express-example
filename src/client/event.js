/**
 *
 * This is our interface to the node 'events' library
 *  *
 */
var $ = require('jquery');
var events = require('events');
var emitter = new events.EventEmitter();

// Signature - create our event object signature from supplied event
var signature = function(e){
	var $configElem = $(e.target).closest('[data-id]');

	//console.log( '$configElem ', $configElem);

	return {
		"id" : $configElem.data('id'),
		"url" : $configElem.data('url'),
		"headline" : $configElem.data('headline'),
		"category" : $configElem.data('category'),
		"originalEvent" : e
	}
};


var attachListeners = function(){
	// Fire events on action buttons

	$('body')
			.on('click', '.action .share', function(e){
				emitter.emit('share', signature(e));
			})
			.on('click', '.action .comment', function(e){
				emitter.emit('comment', signature(e));
			})
			.on('click', '#top-nav .person', function(e){
				emitter.emit('person', signature(e));
			})
			.on('click', '.logout', function(e){
				emitter.emit('logout', signature(e));
			})
			.on('click', '#top-nav .register', function(e){
				emitter.emit('register', signature(e));
			})
			.on('click', '#top-nav .user-profile', function(e){
				emitter.emit('userProfile', signature(e));
			});

};

module.exports.attachListeners = attachListeners;
module.exports.emitter = emitter;
module.exports.signature = signature;

