// Discovery controller

var event = require('./event');
var $ = require('jquery');
var voteTracker = require( './vote-tracker' );


var init = function(){

	var $articleContainer = $('#article-container');
	var $title = $('title');
	
	var callback = function(_data){
		var data = JSON.parse(_data);
		var $data = $(data.html);
		$articleContainer.html($data);
		//console.log('discovery-controller.js :: ', data);
		var pageObj = {
			category : data.category.text,
			contentType : 'discover',
			headline : data.category.title,
			url : data.category.href,
			displayUrl : data.category.href,
			id : data.category.text,
			byline : data.category.text + ' editor'
		};
		event.emitter.emit('discoveryLoaded', pageObj);
	};

	event.emitter.on('loadDiscover', function(url){

		$.ajax({
			url : url,
			headers : {
				'X-content-i100' : 'ajax'
			},
			success : function (data) {
				callback(data, true);
			}
		});
	});

	event.emitter.on('discoveryLoaded', function( pageObj ){

		var state = {
			"type" : "discover",
			"url" : pageObj.url
		};

		window.history.pushState(state, 'i100', pageObj.url);
		// Add tracking for google analytics
		// https://developers.google.com/analytics/devguides/collection/analyticsjs/pages
		if( ga ){
			ga('send','pageview', {
				'location' : window.location.host,
				'page' : window.location.pathname,
				'title' : document.title
			} );
		}

		$title.text( pageObj.category );

		
		var votedIdCollection = voteTracker.getVoted(),
			spotProcessedIds = [];

		for ( var i = 0; i < votedIdCollection.length; i += 1 ) {
			var newId = 'spot-' + votedIdCollection[i];
			spotProcessedIds.push( newId );
		}
		
		spotProcessedIds.forEach(function( item ) {
			var $spotlight = $( 'body' ).find( '#' + item );
			if ( $spotlight ) {
				$spotlight.find('li.upvote').addClass( 'disabled' );
			}
		});

	});




// Add listeners for actions on the card...
	$articleContainer.on('click', '.spotlight a', function (e) {

		e.preventDefault();

		var $spotlight = $(this).closest('.spotlight');

		event.emitter.emit('viewArticle', {
			id : $spotlight.data('id'),
			displayUrl :  $spotlight.data('url'),
			headline : $spotlight.data('headline')
		});
	});
};



module.exports = {
	init : init
};