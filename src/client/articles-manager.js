// articles-manager.js


var event = require('./event');
var $ = require('jquery');

var Article = require('./article-constructor');
var infiniteScroll = require('./infinite-scroll');
var utils = require('./utils');

var listController;
var currentArticleID;
var articles = {}; // This is an object keyed by article id, and the values are our representation of an article object
var currentArticles = []; // list of article IDs currently on page

var articleContainer;
var $title;


var init = function (lc) {

	attachInfiniteScroll();

	$title = $('title');


	//init only happens once kids!

	listController = lc;

	setContainerInfo(); // implicitly (ugh) updates articleContainer

	// get the current article if it exists
	// if it exists, get the id, and see if that id is in the i100 card list
	var primedArticle = checkPrimedArticle();

	// tell the list to show current card (if it's there)
	if(primedArticle) {
		currentArticleID = primedArticle._article.id;
		event.emitter.emit('articleFocus', {id : currentArticleID});
	} else {

		// not strictly true, as user pages will not be primed articles either!
		var $discover = $('#discover, .userpage');

		if($discover.length > 0) {

			var discoverData = $discover.data();
			//console.log('ALL THE DISCO DATA', discover);
			event.emitter.emit('discoveryLoaded', {
				id : discoverData.category,
				displayURL : window.location.href,
				headline : discoverData.title,
				byline : (discoverData.category === "User")? "User account page"  : discoverData.text + ' editor',
				contentType : 'discover',
				releaseDate : new Date()
			});

		} else {
			// is probably a user profile page - so no omniture thing, sorry!
		}
	}

	eventBindings(); // do the event bindings

	return true;
};



var eventBindings = function () {

	// NOTE - Commenting this out as it seems to be breaking the refresh of the list but is not used in any other scenario
	// event.emitter.on('refreshedCards', onRefreshedList);  

	event.emitter.on('viewArticle', function (item, fromPopState) {

		currentArticleID = item.id;
		if(!fromPopState) {
			historyPush({
				id : currentArticleID,
				headline : item.headline,
				displayUrl : item.displayUrl
			});
		}
		initTopArticle(item);
	});

	infiniteScroll.eventBindings(articleContainer);

	// Add listeners for clicking on open / close image captions...
	articleContainer.$el
		.on('click', '.img-container .close', function (e) {
			$(this).closest('figure').removeClass('show-caption')
		})
		.on('click', '.img-container .info', function (e) {
			$(this).closest('figure').addClass('show-caption')
		});

	$(window).on('popstate', popStateHandler);


};


var historyPush = function (ev) {
	//console.log('history push?', ev);
	if(ev.displayUrl == window.location) {
		return false
	}

	var state = {
		type : "article",
		url : ev.displayUrl,
		id : ev.id
	};
	window.history.pushState(state, 'i100', ev.displayUrl);
	// Add tracking for google analytics
	// https://developers.google.com/analytics/devguides/collection/analyticsjs/pages
	if( ga ){
		ga('send','pageview', {
			'location' : window.location.host,
				'page' : window.location.pathname,
				'title' : document.title
		} );
	}
	$title.text(ev.headline);
	event.emitter.emit('articleFocus', ev);

};

var loadNext = function (ev) {
	// TODO Replace this 'next card' stuff to be managed by the scroll handler as is all lazy loading
	var nextCard = listController.getCard(ev.id, 1);

	if(nextCard) {

		getArticleHTML(nextCard, function (data){

			articleContainer.$el.append(data);

			currentArticles.push(nextCard.id);
			var $article = articleContainer.$el.find('#' + nextCard.id);

			setArticleDetails($article);

			// Reattach our Article object to new Dom object
			$('#' + nextCard.id)[0]._article = articles[nextCard.id]._article;

			// Ensure that it is loaded
			articles[nextCard.id]._article.load();


			event.emitter.emit('articleLoad', nextCard);
		});

	}
	// should fire event that list-controller can listen to, to help with scrolling to current card / adding the pointer or 'current' state etc
	// should load  the article into the current top spot in the article column - using the article controller
	// should populate any meta information for things like rel tags and open graph
	// should manage which article is 'current' - for the purposes of omniture integration etc.

};



var popStateHandler = function (e){
	var state = e.originalEvent.state;
	if( !state ){
		return false;
	}
	if( state.type === 'article' ){
		var card = listController.getCard(state.id, 0);
		event.emitter.emit('viewArticle', card, true);
	} else if ( state.type === 'discover' ) {
		event.emitter.emit('loadDiscover', state.url);
	}

};

var setContainerInfo = function () {
	articleContainer = {
		el : document.getElementById('article-container')
	};
	articleContainer.$el = $(articleContainer.el);
	updateContainerInfo();
};

var updateContainerInfo = function () {
	articleContainer.height = articleContainer.$el.height();
};




var checkPrimedArticle = function () {

	var $primedArticle = articleContainer.$el.find('article');

	// If we are on the discovery page it is cool to not have a primed article :)
	if($primedArticle.length < 1) {
		return false; // no article?
	}
	return setArticleDetails($primedArticle);
};


var setArticleDetails = function ($article) {
	var id = $article.data('id');
	if(articles.hasOwnProperty( id )){
		return articles[id];
	}
	var item = new Article($article);


	articles[id] = item;
	return item;
};



// NOTE - Commenting this out as it seems to be breaking the refresh of the list but is not used in any other scenario

// var onRefreshedList = function () {
	
// 	console.log( 'onRefreshedList currentArticleID ', currentArticleID);

// 	if(!currentArticleID){
// 		console.log( '!currentArticleID ', currentArticleID );
// 		return false;
// 	}

// 	console.log( 'AFTA currentArticleID ', currentArticleID );
// 	console.log('articles ', articles );
// 	console.log('articles ', articles._article );


// 	initTopArticle(articles[currentArticleID]._article, true);
// };





var getArticleHTML = function (_article, callback) {

	if(articles[_article.id]) {

		callback(articles[_article.id]._article.$el, false);

		return true;
	}
//	var card = listController.getCard(_article.id, 0);
	$.ajax({
		url : _article.displayUrl,
		headers : {
			'X-content-i100' : 'ajax'
		},
		success : function (data) {
			callback(data, true);
		}
	});
	return false;

};





// initTopArticle - load only the specified article and remove all others

// this can be triggered on load, or on click (ajax).

var initTopArticle = function (item) {

	currentArticleID = item.id;
	//remove everything but the currentArticle from the article container, as the list order may have changed
	var $oldArticlesInDom = articleContainer.$el.find('article').not('#'+currentArticleID);
	$oldArticlesInDom.each(function(i, art){
		articles[$(art).data('id')]._article.unload();
	});

	$oldArticlesInDom.detach();

	articles = {};

	currentArticles = [currentArticleID];


	// If the HTML is already on the page then 'pass'
	var pass = articles.hasOwnProperty(currentArticleID);

	pass = false;
	if(!pass) {


		// no pass? this is triggered by a user clicking on a card.
		getArticleHTML(item, function (data, ajaxed) {
			var $article;
			if(ajaxed) {
				// create article element to simulate the returned item
				$article = $('<div/>').html(data).contents();
			} else {
				$article = data;
			}

			articleContainer.$el.html($article);
			articleContainer.$el.animate({scrollTop: 0}, 600, 'swing');

			var addedArticle = setArticleDetails($article);

			// Reattach our Article object to new Dom object
			$('#' + currentArticleID)[0]._article = articles[currentArticleID]._article;

			addedArticle._article.load();

			// need to prime this with the ads
			event.emitter.emit('articleLoad', item);

		});
	} else {


		// ensure the added article is loaded
		articles[currentArticleID]._article.load();

		event.emitter.emit('articleLoad', _article);
		articleContainer.$el.animate({scrollTop: 0}, 600, 'swing');
	}

};




////////////////////////////////////////////////
//
// Alternative implementation of infinite scroll
//
////////////////////////////////////////////////


// Unload off screen articles, and reload on screen articles
function unloader(){
	var $articles = $('article');
	if($articles.length > 2){
		// find visible articles
		$articles.each(function(){
			// try and find our article representation
			var art = articles[this.id];
			if(!art) return;
			var _article = art._article;
			if(_article.$el && utils.isArticleVisible(_article.$el)){
				// current article - ensure it's visible
				_article.load();
			}
			else{
				_article.unload();
			}
		});
	}
}

function attachInfiniteScroll(){

	var currentFocus = '';
	var currentLoading = '';


	// see if the bottom lazy-load marker is 'visible'
	var checkReadyForNext = function(){
		var $loadMarker = $('.load-marker').last();
		var loadMarkerId = $loadMarker.parent().data('id');
		if(currentLoading !== loadMarkerId && utils.isElementInViewport($loadMarker)){
			currentLoading = loadMarkerId;
			loadNext({ id : loadMarkerId });
		}
	};

	var checkFocus = function(){
		var $focusMarker = $('.focus-marker');


		for(var i = $focusMarker.length; i >= 0; i -= 1) {
			var $marker = $focusMarker.eq(i);
			var id = $marker.parent().data('id');
			if (currentFocus !== id && utils.isElementInViewport($marker[0])) {
				currentFocus = id;
				var _article = articles[id]._article.$el[0];
				var articleData = $(_article).data();
				var ev = {
					id: id,
					displayUrl: articleData.url || '',
					headline: articleData.headline || '',
					byline : articleData.byline || '',
					publishDate : articleData.pubdate || '',
				};
//				console.log("A DATE?", articleData.pubdate);
				historyPush(ev);
				break;
			}
		}
	};

	var checkMarkers = function(){
		checkReadyForNext();
		checkFocus();
		unloader();
	};

	function attachListener(){
		event.emitter.on('articleScroll', checkMarkers);
	}

	attachListener();
}


/////////////////////////////////////////////////////////////////////////////////////





module.exports = {
	init : init
};

