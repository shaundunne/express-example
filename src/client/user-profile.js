// user-profile.js


// These are the gigya interface scripts supplied by the Independent


var $ = require('jquery');



/* global vars */
var gigyaLoggedIn = false;
/* cookie func */
function setCookieG(name,value,days) {
	var date = new Date();
	date.setTime(date.getTime()+(days*24*60*60*1000));
	document.cookie = name + "=" + value + "; expires="+date.toGMTString();
}
function getCookieG(name) {
	var regexp = new RegExp("(?:^" + name + "|;\s*"+ name + ")=(.*?)(?:;|$)", "g");
	var result = regexp.exec(document.cookie);
	return (result === null) ? null : result[1];
}
function deleteCookieG(name) {
	document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
}


var redirectToLogin = function(){
	// Store the current url in a cookie so that on next login it will go back to the page we came from
	setCookieG('gigyaRedirect', window.location.href, 1);

	// redirect
	window.location = '/user/login';
};


var onLogin = function(){
	// test cookie for redirect request
	var redirectTo = getCookieG('gigyaRedirect');
	if(redirectTo){
		deleteCookieG('gigyaRedirect');
		window.location = redirectTo;
	}
};

var init = function(){

	var userStatusUA = new gigya.socialize.UserAction();

	userStatusUA.setTitle('$challengeTitle - $levelTitle');
	userStatusUA.setDescription('*** Challange title: $challengeTitle *** Level title: $levelTitle *** Challange description: $challengeDescription ***');
	userStatusUA.addImage('$badgeURL');


	//
	gigya.gm.showUserStatusUI({
		containerID : 'gigya-user-status',
		userAction : userStatusUA,
		shareParams: {
			showEmailButton: true
		}
	});


	gigya.gm.showAchievementsUI({
		containerID : 'gigya-challenge-status'
	});

	gigya.gm.showLeaderboardUI({
		containerID : 'gigya-leaderboard'
	});

	// Activity Feed Plugin parameters
	var params = {
		containerID: 'gigya-activity' //id of the <DIV> element on the page in which the Plugin should be displayed
		,width:300  //width in px
		,height:400 //height in px

	};

	//API call to load the Activity Feed Plugin
	gigya.socialize.showFeedUI(params);



	/* gigya code */
	gigya.accounts.showScreenSet({screenSet:'Profile-web',containerID:'gigya-account',onAfterSubmit:afterProfileSubmit});

	function afterProfileSubmit(data) {
		document.location = "/";
	}

	// /* -- TODO - START ACTIVITY FEED WIDGET -- */

	// // Constructing a UserAction Object
	// var userActivity = new gigya.socialize.UserAction();
	
	// userActivity.setTitle( '$challengeTitle - $levelTitle' );			// Setting the Title
	// userActivity.setLinkBack("http://www.gigya.com");					// Setting the Link Back
	// userActivity.setDescription("This is my Description");				// Setting Description
	// userActivity.addActionLink("Read More", "http://www.gigya.com");	// Adding Action Link
	// // Adding a Media (image)
	// userActivity.addMediaItem( { 
	// 	type: 'image', 
	// 	src: 'http://www.infozoom.ru/wp-content/uploads/2009/08/d06_19748631.jpg', 
	// 	href: 'http://www.gigya.com' 
	// });
				
	// var activityParams = {
	// 	userAction : userActivity,
	// 	scope : 'internal'
	// };

	// // Publishing the User Action			
	// gigya.socialize.publishUserAction( activityParams );
	
	// /* -- TODO - END ACTIVITY FEED WIDGET -- */







	/* gigya code */



	/* LOGIN  */
	gigya.accounts.showScreenSet({screenSet:'Login-web',containerID:'gigya-login',lastLoginIndication:'none',onAfterSubmit:afterLoginSubmit});

	function afterLoginSubmit(data) {
		if (data.screen == "gigya-login-screen") {
			if (data.response.errorCode == 0) { // successfully logged in
				document.location = "/user/profile";
			} else if (data.response.errorCode == 206001) { // missing required fields
				$('body').on('focus','.gigya-input-text, select', function() {
					$(this).siblings('.tooltip').css('display','block');
				}).on('blur','.gigya-input-text, select', function() {
					$(this).siblings('.tooltip').fadeOut();
				});
			} else if (data.response.errorCode == 403042) { // incorrect details

			}
		} else if (data.screen == "gigya-complete-registiration-screen") {
			document.location = "/user/profile";
		}
	}




	/* REGISTER */


	/* gigya code */
	gigya.accounts.showScreenSet({screenSet:'Login-web',containerID:'gigya-register',startScreen:'gigya-register-screen',lastLoginIndication:'none',onAfterSubmit:afterRegisterSubmit});
	$('body').on('focus','.gigya-input-text, select', function() {
		$(this).siblings('.tooltip').css('display','block');
	}).on('blur','.gigya-input-text, select', function() {
		$(this).siblings('.tooltip').fadeOut();
	});

	function afterRegisterSubmit(data) {
		if (data.screen == "gigya-complete-registiration-screen" && data.response.errorCode == 0) {
			document.location = "/user/profile";
		}
	}


 /*** Register login event handler ***/

 gigya.socialize.addEventHandlers({
			 onLogin:onLogin
		 }
 );



};



module.exports = {
	init : init,
	redirectToLogin : redirectToLogin
};