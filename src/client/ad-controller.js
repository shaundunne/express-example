/**
 * Nirvana ad-controller library
 *
 * The API provides a .init() function for creating a MPU in the
 * specified dom element.
 *
 * This is written in the style of NPM for use with browserify
 */

'use strict';

var event = require('./event');
var $ = require('jquery');

// -- START RE-FACTORED CODE
var INDYES = INDYES || {};
INDYES.injectWallpaperCSS = function (ctrId, inlineCSS) {
	console.log("adding wallpaper to #" + ctrId);
	$('#' + ctrId).append(inlineCSS);
};

var adStore = {};

var stripAdId = function (id) {
	var realId = id.split('-');
	if(realId.length > 1) {
		return realId[1];
	} else {
		return realId[0];
	}
};

var processAdverts = function(){
	// Insert ads 
	googletag.cmd.push(function() {
		
		var unitName = '/71347885/(Core)_The_Independent_UK/Indy_i100',
			mpuCollection = $( '.mpu.unprocessed' ),
			leaderboardMpuClass = 'mpu-leaderboard',
			listMpuClass = 'mpu-card';


		for ( var i = 0; i < mpuCollection.length; i += 1 ) {

			var currentItem = mpuCollection[i];
			var adSlot;
			var unitID = currentItem.id;
			var articleID = stripAdId(unitID);
			console.log('ad-slot', articleID);

			if ( currentItem.classList.contains( leaderboardMpuClass ) ) {

				var articleLeader = googletag.sizeMapping().addSize([1024, 200],[[900, 250],[728, 90]]).addSize([768, 200],[468, 60]).addSize([320, 200], [320, 50]).build();
				adSlot = googletag.defineSlot( unitName , [728, 90], unitID )

				if(!adSlot) {
					googletag.pubads().refresh( [adStore[unitID]] );
				} else {
					adSlot.defineSizeMapping( articleLeader );
					adSlot.addService( googletag.pubads() );
					adSlot.setTargeting( "tile", "leaderboard-1");
					////// MT article-id fix
					adSlot.setTargeting( "articleid", articleID);
					adStore[unitID] = adSlot;
				}
				currentItem.classList.remove( 'unprocessed' );
				googletag.enableServices();
				googletag.display( unitID );
				
				// The second refresh, just to jolt any empty leaderboards into life, mainly aimed at mobile so could do with a conditional here.
				// googletag.pubads().refresh([adStore[unitID]]);

			} else {
				var articleMpu = googletag.sizeMapping().addSize([320, 200],[300, 250]).build();
				adSlot = googletag.defineSlot( unitName , [300, 250], unitID );
				if(!adSlot) {

					googletag.pubads().refresh( [adStore[unitID]] );

				} else {
					adSlot.defineSizeMapping( articleMpu );
					adSlot.addService( googletag.pubads() );
					adSlot.setTargeting( "tile", "mpu-" + i );
					////// MT article-id fix
					adSlot.setTargeting( "articleid", articleID);
					adStore[unitID] = adSlot;
				}
				currentItem.classList.remove( 'unprocessed' );
				googletag.enableServices();
				googletag.display( unitID );
			}
		}
	});
};

// Just leaving this here so that if the race condition re-occurs you can quckly test it out by uncommenting this.
var init = function (){

	// check all functions are available
	if(!googletag || !googletag.cmd || !googletag.sizeMapping){
		console.warn('GTM failed to load');
		return false;
	};

	googletag.pubads().enableAsyncRendering();

	// process ads now
	processAdverts();

	// And on every new list / article load
	// event.emitter.on( 'articleLoad', processAdverts );
	// event.emitter.on( 'discoveryLoaded', processAdverts );
	// event.emitter.on( 'listLoad', processAdverts );
	event.emitter.on('articleLoad', function(){
// console.log( '>>>>>>>>>>> articleLoad fired <<<<<<<<<<' );
		processAdverts();
	});
	event.emitter.on('discoveryLoaded', function(){
// console.log( '>>>>>>>>>>> discoveryLoaded fired <<<<<<<<<<<<<' );
		processAdverts();
	});
	event.emitter.on('listLoad', function(){
// console.log( '>>>>>>>>>>> listLoad fired <<<<<<<<<<<' ); 
		processAdverts();
	});
	
	

};

module.exports = {
	init : init
};
























