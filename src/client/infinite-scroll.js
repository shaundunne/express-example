// infinite-scroll.js
// TODO:  Add all scroll functionality into this library

var event = require('./event');
var $ = require('jquery');
var config = require('./config');
var enabled = config.enableInfiniteScroll;
var scrollStatus = false;

var articleContainer;
var scrollTimer;

var eventBindings = function(_articleContainer){
	articleContainer = _articleContainer;

	articleContainer.el.onscroll = function () {
		triggerScrollEvent();
	};

// thanks to http://joshbroton.com/hooking-up-to-the-window-onscroll-event-without-killing-your-performance/
	scrollTimer = setInterval(function (){
		if(scrollStatus) {
			scrollStatus = false;
			event.emitter.emit('articleScroll', articleContainer.height, articleContainer.el.scrollTop);
		}
	}, config.scrollGranularity);

};

var triggerScrollEvent = function () {
	scrollStatus = true;
};





module.exports = {
	isEnabled : enabled,
	eventBindings : eventBindings
};

