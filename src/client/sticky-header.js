/**
 * Nirvana ad-controller library
 *
 * The API provides a .init() function for creating a MPU in the
 * specified dom element.
 *
 * This is written in the style of NPM for use with browserify
 */

'use strict';

var event = require('./event');
var IScroll = require('./3rdparty/iscroll-probe.js');
var $ = require('jquery');


/**
 * init - Create initialise an ad in the DOM element specified by the supplied ID
 * @param adId
 */
var init = function(){

	event.emitter.on('articleLoad', function(){

		var articleScroll = new IScroll('#article-container',{
				probeType: 3,
				mouseWheel: true
			}),
			el = $('.article-column'),
			stickyHeader = el.find('#sticky-header'),
			socialToolbar = el.find('#inline-social-toolbar'),
			toolbarTopPos = -Math.abs( socialToolbar.position().top ),
			toggleActiveClass = {},
			classAdded = false;

		toggleActiveClass = function(){
			if (articleScroll.y < toolbarTopPos) {
				if ( classAdded === false ) {
					stickyHeader.addClass( 'active' );
					classAdded = true;
				}
			} else {
				if ( classAdded === true ) {
					stickyHeader.removeClass( 'active' );
					classAdded = false;
				}
			}
		};

		articleScroll.on( 'scroll', function(){
			toggleActiveClass();
		});

		articleScroll.on( 'scrollEnd', function(){
			toggleActiveClass();
		});

		document.addEventListener('touchmove', function (e) {
			e.preventDefault();
		}, false);


	});
};


exports.init = init;

