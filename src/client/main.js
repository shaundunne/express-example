/**
 Main.js

 This is our main controller, written using browserify. Find out more at...

 http://browserify.org/


 This is the main controller that calls in all our modules required for our page.

 It is compiled to bundle.js using browserify.  For more information about setting up your dev environment see.

 http://superbigtree.tumblr.com/post/54873453939/introduction-to-browserify

 */
"use strict";


var config = require('./config');


var $ = require('jquery');
// Call our modules....
require('./iOS6hack.js');
require('./cookie-controller.js');
var listActiveStateHandler = require('./list-active-state-handler.js');
var adverts = require('./ad-controller');

// Initialise social
var social = require('./social');


// Initialise stickyHeader
//var stickyHeader = require('./sticky-header');
//stickyHeader.init();

var listController = require('./list-controller');

var articlesManager = require('./articles-manager');
var navigationHandler = require('./navigation-handler');
var discoveryController = require('./discovery-controller');

var attachFastClick = require('./3rdparty/fastclick');
var event = require('./event');

// up the event listener limit...
event.emitter.setMaxListeners(256);


var voteTracker = require('./vote-tracker');

var listRefreshController = require('./list-refresh-controller');

// Tracking manager - needs to be init'd early to ensure props are updated
// _prior_ to updating the url (which auto triggers an omniture call)

var trackingManager = require('./tracking-manager');

var gamification = require('./gamification');

var userProfile = require('./user-profile');

// Things that rely on the document being ready...
$(document).ready(function () {

	social.init();
	trackingManager.init();
	// Initialise list control
	listController.init();
	articlesManager.init(listController);// Initialise article control
	navigationHandler.init();
	voteTracker.init();
	
	// Initialise listActiveStateHandler
	listActiveStateHandler.init();

// use fast click...
	attachFastClick(document.body);

	listRefreshController.init();
	discoveryController.init();



/*

	// if an article is loaded then trigger article load event
	var $article = $('article');
	if($article.length){
		event.emitter.emit('articleLoad', {id : $article.data('id')});
	};*/


	// attach all the listeners defined in the event.js library
	event.attachListeners();
	setTimeout(adverts.init, 1000);
	gamification.init();
	userProfile.init();
});


