/**
 * Nirvana social toolbar controller
 *
 * This script accesses the Gigya social share API to add a share toolbar to the page.
 * http://developers.gigya.com/010_Developer_Guide/18_Plugins/015_Share_Bar
 * http://developers.gigya.com/020_Client_API/010_Socialize/socialize.postBookmark
 *
 *
**/

'use strict';
var event = require('./event');
var $ = require('jquery');

var hiddenElmSelector = '.share-box',
	socialToolbar,
	id,
	commentID;

var ShareBar = function( $article, _id ) {
	id = _id,
	commentID = 'comment-' + id,
	socialToolbar = $article.find('.social-toolbar');
	// Attach Gigya links to social media icons

	socialToolbar.on( 'click', '.link',{ "id" : id, "commentID" : commentID }, socialLinkSetup );
	socialToolbar.on( 'click', '.share', toggleMenu );

};

var socialLinkSetup = function( evnt ) {
	var el = $(this),
		config = el.data( 'shareConfig' ) ? el.data( 'shareConfig' ) : {},
		articleElm = el.closest( '#' + evnt.data.id ),
		postUrl = articleElm.data( 'url' ) ? articleElm.data( 'url' ) : ' ',
		provider = el.data( 'provider' ) ? el.data( 'provider' ) : ' ',
		title = config.title ? config.title : ' ',
		thumbnailURL = config.socialTeaserImg ? config.socialTeaserImg : ' ';

	//console.log( 'thumbnailURL  ', thumbnailURL );

	// Set up the object for the social media links
	var params = {
		// This is needed to link back to the article
		url : postUrl,
		provider : provider,
		title : title,
		thumbnailURL : thumbnailURL
	};
	gigya.socialize.postBookmark(params);

	event.emitter.emit('share', event.signature(evnt) );

	return;
};

var toggleMenu = function( evnt ) {
	if ( evnt ) {
		evnt.stopPropagation();
	}
	var hiddenElem = socialToolbar.find( hiddenElmSelector ),
		activeClass = 'is-visible';

	hiddenElem.toggleClass( activeClass );
	// Close the hidden menu if a click on the body is registered
	$('html').on('click', function(){

		hiddenElem.removeClass(activeClass);
	
	});

	return;
};
module.exports = ShareBar;





















