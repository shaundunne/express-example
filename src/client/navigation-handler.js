/**
 * Created by leighgarland on 28/04/2014.
 */

"use strict";
var $ = require('jquery');
var event = require('./event.js');


// Cool scroll features...
var IScroll = require('./3rdparty/iscroll-probe.js');



var scroller = function(element) {
	var options = {
		mouseWheel : true,
		tap : true,
		bounce : false
	};
	return new IScroll(element, options);
};


var init = function () {

	var $topElement = $('html');
	var $topNav = $('#top-nav');
	var hamburgerScroller;
	var $drawer = $('#drawer');
	var $listWrapper = $drawer.find('#list-wrapper');
	// Add top nav listeners

	event.emitter.on('topMenuClose', function(){
		$topElement.removeClass('top-menu-active')
	});

	event.emitter.on('topMenuOpen', function(){
		$topElement
				.removeClass('user-menu-active')
				.addClass('top-menu-active');
		if(hamburgerScroller){
			hamburgerScroller.scrollTo(0, 0);
		}
	});

	// Close drawer when an article is viewed on mobile
	event.emitter.on('viewArticle',function(){

		$topElement.removeClass('drawer-open');
		// De-select top nav items when new article loads
		$('#top-nav .active').removeClass('active');
	});

	/*
		DEV NOTE
		This thing below feels a bit horrid as we are sniffing the user agent.
		But it was done to fix this: http://devops.niceagency.co.uk:8080/browse/NIRVANA-106
		I tracked the click event and it fires, but it was like something was blocking 
		the url from being followed. 
		This only happens on Android so unfortunately we have to detect the user agent 
		and then force the issue.
		Have tested this on Nexus 4, Samsung S4, iPhone5, iPad Mini.
		All seems to be fine so Im commiting it.
	*/
	var ua = navigator.userAgent.toLowerCase();
	var isAndroid = ua.indexOf("android") > -1 && ua.indexOf("mobile");
	var clickTapEvent = (isAndroid) ? 'tap' : 'click';

	if(isAndroid) {
		$('.footer-menu').on( 'tap', '.menu-item a', function( e ){
			var link = e.currentTarget.href;
			window.open( link, '_blank' );
		});
	}

	$topNav.find('.menu').on( 'click', function () {
		event.emitter.emit($topElement.hasClass('top-menu-active') ?  'topMenuClose' : 'topMenuOpen');
	});

	$topNav.find('.menu-item.selectable').on( clickTapEvent, function (originalEvent) {
		originalEvent.preventDefault();
		$('#top-nav .active').removeClass('active');
		$(this).addClass('active');
		event.emitter.emit('topMenuClose');
		event.emitter.emit('loadDiscover', originalEvent.target.href);
	});

	// Add drawer menu listeners
	$('#drawer-bar').on('click', function () {
		$topElement
			.removeClass('top-menu-active')
			.removeClass('user-menu-active')
			.toggleClass('drawer-open');
		if($topElement.is('.drawer-open')) {
			$listWrapper.css('display', 'inherit');
		}
	});


	// Add scrolling to hamburger menu
	hamburgerScroller = scroller('#top-nav .inner-wrapper');

	// if we are at the root of the app
	if ( window.location.href === window.location.origin + '/' ) {
		// open the drawer on first load
		$topElement.addClass('drawer-open');
	}

	var logoWrapper = document.getElementById( 'home-logo' );
		
	$( logoWrapper ).on('click', '.logo_i100_beta', function( evnt ){
		event.emitter.emit('loadDiscover', '/discover' );
	});




	/*
	 * Have to detect the drawer open / close CSS transitions, because we need to kill the DOM or something
	 * http://davidwalsh.name/css-animation-callback
	 * */

	/* NIRVANA-138 */

	function whichTransitionEvent(){
		var t;
		var el = document.createElement('fakeelement');
		var transitions = {
			'transition':'transitionend',
			'OTransition':'oTransitionEnd',
			'MozTransition':'transitionend',
			'WebkitTransition':'webkitTransitionEnd'
		};

		for(t in transitions){
			if( el.style[t] !== undefined ){
				return transitions[t];
			}
		}
	}

	var transitionEvent = whichTransitionEvent();
	transitionEvent && $drawer[0].addEventListener(transitionEvent, function() {
		if($topElement.is('.drawer-open')){
		} else {
			$listWrapper.css('display', 'none');
		}
	});


};

module.exports = {
	init : init
};






















