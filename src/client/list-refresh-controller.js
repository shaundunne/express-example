/**
 * List refresh controller
 *
 * This is written in the style of NPM for use with browserify
 */

"use strict";
var $ = require('jquery'),
	event = require('./event');

var init = function(){
	refreshEventTrigger();
};


var refreshEventTrigger = function(){
	var drawer = $('#drawer'),
		refreshTrigger = drawer.find('#refreshTrigger'),
		listContainer = drawer.find('#list-container');
	
	refreshTrigger.on('click', function( evnt ){
		evnt.stopPropagation();
		event.emitter.emit( 'refreshListRequest' );
	});

};

module.exports = {
	init : init
}
