/*

 Hack for getting more real estate on iPhones running iOS6..

 refs:
 http://remysharp.com/2010/08/05/doing-it-right-skipping-the-iphone-url-bar/
 http://ianrichard.com/2012/08/hide-mobile-safari-browser-chrome/

    n.b. Apple 'fixed' this in iOS7, so we also have the 'minimal-ui' meta tag for this

 */
/(iPhone|iPod).*(OS 6)/i.test(navigator.userAgent) && setTimeout(function () {
    var mainContainer = document.querySelector("#main-container");
    mainContainer.style.position = "relative";
    mainContainer.style.height = (screen.height - 60) + 'px';
    window.scrollTo(0, 1);
    setTimeout(function(){
        mainContainer.removeAttribute('style'); // remove our hacks
    },0);
}, 100);


// We might be able to do something similar for chrome on Android, but it is a bit too glitchy...

/*
/(Android).*(Chrome)/i.test(navigator.userAgent) && setTimeout(function () {
    var mainContainer = document.querySelector("#main-container");
    mainContainer.style.position = "relative";
    var statusBarHeight = (screen.height <= 320) ? 20 : 25;
    mainContainer.style.height = (screen.height - statusBarHeight) + 'px';
}, 100);
*/