"use strict";
var $ = require('jquery'),
	event = require('./event'),
	init,
	scrollToArticle,
	removeClass,
	getarticleIDListItem,
	getScrollTopDestination;
	
init = function () {
	event.emitter.on('articleFocus', scrollToArticle);
	event.emitter.on('scrollToArticle', scrollToArticle);
};

scrollToArticle = function ( article ) {

	var $listContainer = $('#list-container');
	var articleIdSelector = '#card-' + article.id,
		activeClass = 'is-selected',
		cardCollection = $listContainer.find( '.card' ),
		cardMarginBottom = parseInt( window.getComputedStyle( cardCollection[0],false ).marginBottom ),
		scroller = $('#drawer'),
		currentListItem,
		scrollTopDestination,
		currentScrollTop = scroller.scrollTop();

	// Remove active class
	removeClass( cardCollection, activeClass );

	// Get current article list index
	currentListItem = getarticleIDListItem( cardCollection, article);

	// Get new scroll top destination
	scrollTopDestination = getScrollTopDestination( currentListItem, currentScrollTop, cardMarginBottom );

	// Scroll column to current item index in list
	$listContainer.find( articleIdSelector ).addClass( activeClass );

	// Set scroll parent to new destination
	scroller.animate({scrollTop:scrollTopDestination}, '500', 'swing');
};

removeClass = function( cardCollection, activeClass ){
	for ( var i = 0; i < cardCollection.length; i += 1) {
		if ( cardCollection[i].classList.contains( activeClass ) ) {
			cardCollection[i].classList.remove( activeClass );
			return;
		}
	}
	return false;
};

getarticleIDListItem = function( cardCollection, article ){
	for ( var i = 0; i < cardCollection.length; i += 1) {
		if ( cardCollection[i].id === 'card-' + article.id ) {
			return cardCollection[i];
		}
	}
};

getScrollTopDestination = function( currentListItem, currentScrollTop, cardMarginBottom ){
	var position = $( currentListItem ).position();
	if( position ){
		return position.top  + ( currentScrollTop - ( cardMarginBottom / 2 ) );
	}
	else {
		return false;
	}
};

module.exports = {
	init : init
};





















