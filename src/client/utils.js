// utils.js

var $ = require('jquery');
var config = require('./config');

var windowHeight = $(window).height();

function isElementInViewport (el) {
	//special bonus for those using jQuery
	if (el instanceof $) {
		el = el[0];
	}
	if(typeof el === 'undefined') return;

	var rect = el.getBoundingClientRect();

	return (
			rect.top >= 0 &&
			rect.left >= 0 &&
			rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
			rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
			);
}

function isArticleVisible (el) {
	if(!el) return;
	//special bonus for those using jQuery
	if (el instanceof $) {
		el = el[0];
	}

	var tolerance = config.scrollArticleUnloadFactor * windowHeight; // this tolerance senses when the article is visible, or close to visible


	var rect = el.getBoundingClientRect();


	return (rect.top <= $(window).height() + tolerance && rect.bottom >= -tolerance)
}


module.exports = {
	isElementInViewport : isElementInViewport,
	isArticleVisible : isArticleVisible
};