/**
 * Article constructor
 *
 * This represents an article on the page.  It is responsible for listening to
 * events that affect its own status, and updating itself accordingly

 * */

"use strict";
var $ = require('jquery');
var event = require('./event');
var voteTracker = require('./vote-tracker');
var Comment = require('./comment-controller');
var ShareBar = require('./social-tool-bar');


// How this should work

	// first it should check to see if there's an existing article (there might be one on load) and create an object to represent it

	// the article-handler is not responsible for knowing what's 'next' or 'previous' - that's the job of the list-controller


var Article = function ($article) {
	var self = this;

	this.html = '';
	this._article = {};
	this._article.$el = $article;
	this._article.id = $article.data('id');
//	getPositioning(this); // aaaagh more implicit changes :(

//	this._article.getScrollMarkers = getScrollMarkers;

	this._article.isEmpty = false;

	// instantiate a comment controller object
	this._article.comment = new Comment(self._article.$el, this._article.id);

	var $upvote = self._article.$el.find('.upvote');

	// function to update dom representation of voted cards
	var toggleMarkVoted = function(voted){
		$upvote.toggleClass('disabled', voted);
	};


	this._article.shareBar = new ShareBar( self._article.$el, this._article.id );


	// mark as voted if this has been previously voted upon
	if(voteTracker.hasVoted(this._article.id)){
		toggleMarkVoted(true);
	};


	// loader method - this grabs html back from temporary storage
	// if this article has previously been 'unloaded'
	this._article.load = function(){
		if(!self.isEmpty) { return; }
		// replace the html with temporary storage version
		self._article.$el.html(self.html);
		// unfix the height
		// (n.b. it might be better to attach this to a click event on the article to avoid unnecessary repaints)
		self._article.$el.attr('style','');
		self.isEmpty = false;
	};

	// unloader method - this stores the html in temporary storage
	// to reduce the amount of dom elements on the page.  It also
	// fixes the height of the article so that the empty article does
	// not collapse and affect the scroll position
	this._article.unload = function(){
		if(self.isEmpty) {return; }
		// fix the container height
		self._article.$el.height(self._article.$el.height());
		// place the html into temporary storage
		self.html = self._article.$el.html();
		self._article.$el.html('');
		self.isEmpty = true;
	};

	// console.log( 'Article constructor self ', self );

	return this;
};



module.exports = Article;
