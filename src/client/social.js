/*
 Social control
 */
"use strict";
var event = require('./event');

var $ = require('jquery');


var $topElement = $('html');

var user;

var loggedIn = false; // track whether user is logged in

// Set default placeholders for the photo and thumbnail urls
var defaultPhoto =  "http://cdn.gigya.com/site/images/bsAPI/Placeholder.gif";

var thumbnailURL = defaultPhoto;
var photoURL = defaultPhoto;


var onCloseLogin = function (e) {
	$topElement.removeClass('modal');
	getUserInfo();
};


var isLoggedIn = function(){
	return loggedIn;
};




var showLoginScreen = function(initialScreen) {
	$topElement.addClass('modal');
	var options = {
		screenSet: 'Login-web' ,
//		mobileScreenSet: 'Mobile-login',
		onHide: onCloseLogin
	};

	if(initialScreen){
		options.startScreen = initialScreen;
	}
	gigya.accounts.showScreenSet(options);
};



var getImage = function(res, field){
	if(res.user[field] && res.user[field].length > 0){
		return res.user[field]
	}
	return defaultPhoto;
};

// setUserInfo - callback on login request
var setUserInfo = function (res) {
	if (res.user != null && res.user.isConnected) {
		loggedIn = true; // track user login status
		user = res.user;
		//console.info("User '%s' is logged in.", user.nickname);
		thumbnailURL = getImage(res, 'thumbnailURL');
		photoURL = getImage(res, 'photoURL');
		event.emitter.emit('userLoggedIn');
	}
};

// get user info
var getUserInfo = function () {
	gigya.socialize.getUserInfo({ callback: setUserInfo })
};


// initial script
var init = function () {
	var noFn = function(){};
	if(!window.gigya){
		console.error('Unable to load gigya');
		// TODO Stub out gigya functions we use so that we can run without gigya...
		window.gigya = {
			comments : {
				showCommentsUI : noFn
			},
			ds : {
				get : noFn
			},
			socialize : {
				postBookmark : noFn
			}
		}
		return;
	}
	eventBinding();
	getUserInfo();
};

var eventBinding = function () {

// Handle clicks on the person icon
	event.emitter.on('person', function ( e ) {

		if(!loggedIn){
			showLoginScreen();
		}
		else{
			event.emitter.emit('userProfile');
		}
	});

// Handle logout
	event.emitter.on('logout', function (e) {
		gigya.accounts.logout({
			callback : function(){
				event.emitter.emit('userLoggedOut');
			}
		});
	});

// Handle logout success
	event.emitter.on('userLoggedOut', function (e) {
		$topElement
			.removeClass('logged-in')
			.removeClass('user-menu-active');
		loggedIn = false;
	});
//
//
//	event.emitter.on('share', function (e) {
//		console.log('Share detected on %s', e.id);
//	});
//
//	event.emitter.on('comment', function (e) {
//		console.log('Comment detected on %s', e.id);
//	});

// add a top level dom class when user has logged in
	event.emitter.on('userLoggedIn', function (e) {
		$topElement.addClass('logged-in');
		$('.avatar-thumbnail').css({backgroundImage : "url('" + thumbnailURL + "')"})
		$('.avatar-photo').css({backgroundImage : "url('" + photoURL + "')"})
		$('.nickname').html(user.nickname)
	});

// add a top level dom class to show user menu
	event.emitter.on('userProfile', function (e) {
		$topElement.removeClass('top-menu-active');
		$topElement.toggleClass('user-menu-active');
	});

};

//




module.exports = {
	init : init,
	isLoggedIn : isLoggedIn
};



















