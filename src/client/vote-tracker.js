/**
 *  vote tracker...

 This is implemented in order to persist the action of voting on an article via the Gigya data store API.

 *
 * It uses two schemas.  One is used to store the user's previous votes in order to mark them as voted.
 * This is a bit like a server-side cookie
 *

  Schema name : 'userdata'

  "voted": {
                "writeAccess": "clientCreate",
                "arrayOp": "push",
                "type": "boolean",
                "allowNull": true
            }


 **  Article Interaction
 * used to store all article interactions, e.g. upvoting etc.
 * This will be what the server uses to track voting

 Schema name : 'articleInteraction'

 "voteValue": {
                "writeAccess": "clientCreate",
                "arrayOp": "push",
                "type": "integer",
                "allowNull": true
            }

 *
 */

// Load dependencies
var $ = require('jquery');
var event = require('./event');
var emitter = event.emitter;
var social = require('./social');


// These are the names of the events we'll use in this library
var eName = {
	onBeforeVote : 'beforeVote',
	onAfterVote : 'vote',
	onBeforeRevokeVote : 'beforeRevokeVote',
	onAfterRevokeVote : 'revokeVote'
};




var voted = []; // array of recent items that have been voted apon
var voteStoreLimit = 200; // limit the above array size for performance reasons



// Data schema names...
var voteSchema = 'articleInteraction';
var userSchema = 'userdata';

// Vote tracker key - used to set/retrieve a single record relating to previous votes for the current user
var userKey = 'userdata';


// Register DOM event listeners
var registerDomListener = function(){
	$('body')
			.on('click', '.upvote:not(.disabled)', function(e){
				var $target = $(e.target);
				$target.addClass('pending');

				emitter.emit(eName.onBeforeVote, event.signature(e) );

			})
			.on('click', '.upvote.disabled', function(e){
				var $target = $(e.target);
				$target.addClass('pending');
				
				emitter.emit(eName.onBeforeRevokeVote, event.signature(e) );

			});
};

// listen for vote events (probably emitted by the dom listener above)

emitter.on(eName.onBeforeVote, function (e) {
	if (!social.isLoggedIn()) {
		// user may not be logged in
		event.emitter.emit('person', e);

		// on login we'll retry the vote
		event.emitter.on('userLoggedIn', function(){
			event.emitter.emit(eName.onBeforeVote, e);
		});
	}
	else {
		// user is logged in
		// check that this can be voted on
		if(!hasVoted(e.id)){
			registerVote(e.id, 1)
		}
	}
});

// listen for revokevote events

emitter.on(eName.onBeforeRevokeVote, function (e) {
	if (!social.isLoggedIn()) {
		// user may not be logged in
		event.emitter.emit('person', e);

		// on login we'll retry the vote
		event.emitter.on('userLoggedIn', function(){
			event.emitter.emit(eName.onBeforeRevokeVote, e);
		});
	}
	else {
		// check that this has a vote to revoke
		if(hasVoted(e.id)){
			registerVote(e.id, 0)
		}
	}

});


// hasVoted - has the current user previously voted on an article with the specified id???
var hasVoted = function(id){
	return voted.indexOf(id) > -1;
};


// registerVoteCallback
// Process the response from Gigya...
var registerVoteCallback = function(res){
	if(res.errorCode === 0){
		var id = res.oid;
		if(hasVoted(id)){
			processUpvote(id);
		}
		else {
			processRevokeUpvote(id);
		}
	}
};

// registerVote - record a vote action against the specified id
// This is the real deal, and what will be counted by the server
// The vote value should be 1 for vote, and 0 for revoke vote
var registerVote = function(id, value){
	// Send the actual vote to gigya...
	var params = {
		data: { voteValue : value },
		oid: id,
		type: voteSchema,
		callback: registerVoteCallback
	};
	gigya.ds.store(params);
	trackVote(id, value);
};


//  Stuff to do with vote tracking for current user (not actual voting)



// Callback on trackVote
var trackvoteCallback = function(res){
	if(res.errorCode !== 0){
		console.warn('Error storing recent vote history', res);
	}
};

// trackVote - this tracks the most recent votes
// in a convenient format within gigya, so we can retrieve it in one hit
var trackVote = function(id, value){

	if(!hasVoted(id) && value === 1){
		// track this vote locally...
		voted.push(id);
	}
	if(hasVoted(id) && value === 0){
		// Remove this vote from local tracking...
		voted.splice(voted.indexOf(id), 1);
	}

	// Ensure that the number of tracked votes is not over the limit...
	if(voted.length > voteStoreLimit) {
		voted.shift();
	}

	// Send to gigya all votes tracked as a comma separated string
	var params = {
		data: { voted : voted.join(',') },
		oid: userKey,
		type: userSchema,
		callback: trackvoteCallback
	};

	gigya.ds.store(params);

};



// Emit an upvoteSuccess event for given id
var processUpvote = function(id){
	// mark cards and articles as upvoted
	$('#' + id + ' .upvote, #spot-' + id + ' .upvote').addClass('disabled');
	// tell the card it has been upvoted
	$('#card-' + id).each(function(){this.card.upvote();});

	emitter.emit(eName.onAfterVote, {id : id});
};


// Emit an upvoteSuccess event for given id
var processRevokeUpvote = function(id){
	// remove vote pending flags
	$('#' + id + ' .upvote, #spot-' + id + ' .upvote').removeClass('disabled');
	$('#card-' + id).each(function(){this.card.revokeVote();});
	emitter.emit(eName.onAfterRevokeVote, {id : id});

};



// getVotesCallback - called after Gigya returns list of previous votes
var getVotesCallback = function(res){
	if(res.errorCode === 0 && res.data && res.data.voted){
		voted = res.data.voted.split(',');
		voted.forEach(processUpvote);
	}
};


// get all previous votes from Gigya
var getVotes = function(){

	gigya.ds.get({
		type: userSchema,
		oid: userKey,
		callback: getVotesCallback
	});
};


// revoking vote is the same as registering vote value 0
var revokeVote = function(id){
	registerVote(id, 0)
};


// Ask Gigya which articles the current user has voted on
var init = function(){
	getVotes();
	registerDomListener();
};

var getVoted = function(){
	return voted;
};

// public interface...
module.exports = {
	hasVoted : hasVoted,
	registerVote : registerVote,
	revokeVote : revokeVote,
	init : init,
	getVoted : getVoted
};

