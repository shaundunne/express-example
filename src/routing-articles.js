var config = require('./config');
var db = require('./db-interface');
var globals = require('../../config/global');
var express = require('express');
var renderMarkdown = require('./article-handler.js');
var tracking = require('./tracking');
var auth = require('./auth');
var deviceCheck = require('./device-check');
var discoveryHandler = require('./discovery-handler');
var err404 = require('../../config/404_article.js');


var urlCheck = function (reqUrl) {

	var realIdArr = reqUrl.split('--');
	return realIdArr[1] || realIdArr[0];
};


var isAjax = function (req){
	return req.header('X-content-i100') === 'ajax'
};



var articleRouting = function (app){

	// more specific requests should be dealt with first, leaving anything else for the end.

	app.get('/article/:id?', function (req, res) {

		// because this could be the first time someone has visited the site
		var device = deviceCheck.layoutCheck(req);

		if(device.layout === 'cookie-check') {
			res.render(device.layout, {globals : globals});
			res.end();
			return false;
		}

		// because this is definitely requesting an article - go and get the article contents
		var id = urlCheck(req.params.id);

		db.getArticleJSON(id)
			.then(function (articleData) {

				//console.log('SHOULD IT MATCH?', globals.settings.server.WEB + req.originalUrl, articleData.displayURL);

				//check to see if url matched displayURL
				if(globals.settings.server.WEB + req.originalUrl !== articleData.displayURL) {
					// Send 301, with correct URL
					console.log('++++++++ 301 THAT', articleData.displayURL);
					res.setHeader('Location', articleData.displayURL);
					res.status(301);
					res.end();
					return false;
				}



				// do the markdownConversion
				var article = renderMarkdown(articleData, device.vwidth);

				//console.log('Now render full article', article.dbid);
				// check to see if it's an AJAX
				// if it is, just return article HTML
				// if not, return whole page
				if(isAjax(req)) {
					res.type('text/html');



					if(article.status === 'draft') {

						// This one is a pretty unlikely edge case, as the user would have had to have triggered an AJAX request for this article, which should only be doable if a link has been displayed on the page
						console.warn('DRAFT ARTICLE REQUESTED!', article.status);
						res.status(404);
					//	err404.body = "Sorry, that article is not currently available to the public.";
						res.render(device.layout, {globals : globals, article : err404, tracking : tracking(err404)});
						res.end();
						return false;
					}


					//render article (combines markdown and headline, hero etc.)
					res.render('snippets/article', {globals : globals, article : article, tracking : tracking(article)});
					// send entire article response
					res.end();
					return true;
				}

				// render the full page

				if(article.status === 'draft') {


					res.status(404);
				//	err404.body = "Sorry, that article is not currently available to the public.";
					res.render(device.layout, {globals : globals, article : err404, tracking : tracking(err404)});
					res.end();
					return false;
				}

				res.render(device.layout, {globals : globals, article : article, tracking : tracking(article)});
				res.end();

			}, function (err){
				console.log('RENDERING ERROR?', err);
				res.status(404);
//				err404.body = "Sorry, we're not sure what you were looking for.";
				res.render(device.layout, {globals : globals, article : err404, tracking : tracking(err404)});
				res.end();
			});

	});

	// Category routing

	globals.categories.forEach(function(category){
		app.get(category.href, function (req, res) {
			var device = deviceCheck.layoutCheck(req);
			discoveryHandler(app, req, res, category, isAjax(req), device);
		});
	});

	// Discovery routing
	app.get('/' ,function (req, res) {
		var device = deviceCheck.layoutCheck(req);
		discoveryHandler(app, req, res, globals.categories[0], isAjax(req), device);
	});


	// User page routing

	var userPages = [ 'login', 'profile', 'register', 'account' ];

	userPages.forEach(function(page){
			app.get('/user/' + page ,function (req, res) {
				var device = deviceCheck.layoutCheck(req);
				var trackingObj = {
					headline : 'User ' + page + ' page',
					pageType : page,
					displayURL : '/user/' + page
				};
				var trackThis = tracking(trackingObj);
				var layoutData = {
					globals : globals,
					userpage : page,
					tracking : trackThis
				};
				res.render(device.layout, layoutData);
				res.end();
			});
		}
	);


	return app;

};

module.exports = articleRouting;