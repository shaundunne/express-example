// Tracking

// Configure the Omniture object for passing into the foot of the page (via the omniture.jade template)

var moment = require('moment');

var tracking = function(_trackingObj){
	var trackingObj = _trackingObj || {};
	// Set up tracking variables
	var pageName = trackingObj.headline ? trackingObj.headline.replace('"',"'") : trackingObj.id || 'no page name';
	var itemDate = trackingObj.releaseDate || trackingObj.publishDate || trackingObj.pubDate || '';
	var displayDate = moment(itemDate).format('dddd, D MMMM YYYY');


	//console.log("THE DATE", itemDate);
	var category =  trackingObj.category || 'no category';
	var pageType = '';  // populate only on 404 pages with 'errorPage'
	var byline = trackingObj.byline || 'no byline';
	var contentType = trackingObj.contentType || 'article'; // populate with content type e.g. frontpage, channel front, article, gallery etc;
	// Return the object
	var displayUrl = trackingObj.displayURL || trackingObj.displayUrl || trackingObj.url || 'noDisplayURL';
	var urlArray = displayUrl.split('/');
	var lastUrlPart = urlArray[urlArray.length-1];

	return {
		pageName : pageName,
		server : 'i100.independent.co.uk',
		channel : category,
		heir1 : category,
		pageType : pageType,
		prop11 : pageName,
		prop12 : lastUrlPart,
		prop13: byline,
		prop16 : displayDate

	}



};

module.exports = tracking;