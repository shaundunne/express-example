
var auth = require('./auth');
var global_config = require('../../config/global.js');
var db = require('./db-interface');

var moment = require('moment');


var miscRouting = function (app){

	//auth(app);

// Something to do with sitemaps and the googles
	app.get('/sitemap_news.xml', function(req, res){


		db.getSiteMapArticles()
			.then(function (articles) {

				articles.forEach(function (article) {
					article.keywords = article.tags.join(',');
					article.formattedDate = moment(new Date(article.releaseDate)).format('YYYY-MM-DD')
				});

				var siteMapObj = {
					global : global_config,
					articles : articles
				};
				res.header('Content-Type', 'application/xml');
				res.render('sitemap_news', siteMapObj);
//		res.send('This is the news...');
				res.end();
			}, function (err){
				//errror!
				console.error('THERE WAS AN ERROR: getSiteMapArticles() Failed for some reason', err);
			});









	});

	return app;

};

module.exports = miscRouting;