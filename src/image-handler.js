/**
 * Created by tim on 18/03/2014.
 */

var fs = require('fs');
var gm = require('gm');
var image = {};
var imagePath = __dirname.replace('/WEB/src','/DB/temp');

image.getPath = function(){
    return imagePath;
};

image.process = function(options){
    if(!options.filename){return(error(null, options.res, {statusCode:500, err:'Invalid image url'}))}
    console.log('get image %s and resize to %sx%s', options.filename, options.width, options.height);
    var originalImage = imagePath + '/' + options.filename;

    gm(originalImage)
       .resize(options.width, options.height)
       .noProfile()
       .toBuffer(function (err, buffer) {
           if (!err) options.res.end(buffer);
           else console.error(err);
       });

};


var error = function (req, res, err) {
    console.error("An error occurred: " , err.err);
    res.status(err.statusCode || 404);
    res.type('application/json');
    res.end(JSON.stringify(err.status));
    return err;
};

module.exports = image;