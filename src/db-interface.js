
var global_config = require('../../config/global.js');
var http = require('http');
http.globalAgent.maxSockets = 10000;
console.log('WEB/DB-INTERFACE http.globalAgent.maxSockets = ', http.globalAgent.maxSockets);
var db = {};

var Q = require('q');





/*
* TODO
*
* Refactor this, so that it's a promise, and returns the content of the article, for the renderer.
*
*
* */

var getResp = function (url, deferred) {

    http.get(url, function(dbRes){

		dbRes.setEncoding('utf-8');

		var responseString = '';

		dbRes.on('data', function(data) {
			responseString += data;
		});

		dbRes.on('end', function() {
			if (dbRes.statusCode === 200){
				deferred.resolve(JSON.parse(responseString));
			} else {
				console.error('##################### ERROR! dbRes.statusCode = ', dbRes.statusCode);
				deferred.reject({
					err : 'ERROR!',
					statusCode : dbRes.statusCode
				});

			}
		});

		dbRes.on('error', function(error) {
			console.log('AN ERROR OCCURRED', error);
			deferred.reject({
				err : error,
				statusCode : dbRes.statusCode
			});
		});


	});
};

var getArticleJSON = function(id){

	var deferred = Q.defer();

    var url = global_config.settings.server.DB + '/article/' + id;

    getResp(url, deferred);

	return deferred.promise;

};

var getSiteMapArticles = function () {
	var deferred = Q.defer();

	var url = global_config.settings.server.DB + '/archive/' + global_config.sitemap.days + '?options=tags,images,releaseDate';

	getResp(url, deferred);

	return deferred.promise;

};

var getDiscoveryArticles = function (section) {
	var deferred = Q.defer();

	var url = global_config.settings.server.DB + '/discover' +  '/' + section + '?options=images,byline';

	getResp(url, deferred);

	return deferred.promise;

};



module.exports = {
	getArticleJSON : getArticleJSON,
	getSiteMapArticles : getSiteMapArticles,
	getDiscoveryArticles : getDiscoveryArticles
};