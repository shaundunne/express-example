var config = require('./config.js');
var images = [];
var vwidth = null;
/**
 *
 * Markdown customisations for Nirvana
 */


/**
 * These helper functions have been copied verbatim from the Markdown.Converter.js
 */

function attributeEncode(text) {
    // unconditionally replace angle brackets here -- what ends up in an attribute (e.g. alt or title)
    // never makes sense to have verbatim HTML in it (and the sanitizer would totally break it)
    return text.replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
}


function escapeCharacters(text, charsToEscape, afterBackslash) {
    // First we have to escape the escape characters so that
    // we can build a character class out of them
    var regexString = "([" + charsToEscape.replace(/([\[\]\\])/g, "\\$1") + "])";

    if (afterBackslash) {
        regexString = "\\\\" + regexString;
    }

    var regex = new RegExp(regexString, "g");
    text = text.replace(regex, escapeCharacters_callback);

    return text;
}


function escapeCharacters_callback(wholeMatch, m1) {
    var charCodeToEscape = m1.charCodeAt(0);
    return "~E" + charCodeToEscape + "E";
}



/**
* The following functions are customised to suit our needs, but retain the same names as their
 * original counterparts.
 */



function writeImageTag(wholeMatch, m1, m2, m3, m4, m5, m6, m7, m8) {
    var whole_match = m1;
    var alt_text = m2;
    var link_id = m3.toLowerCase();
    var url = m4;
    var credits = m7; // replaces title
    var caption = m8; // added by Nirvana

    var figAttr = '';

    if (url == "") {
        return whole_match;
    }

    alt_text = escapeCharacters(attributeEncode(alt_text), "*_[]()");

    var image = getImageByUrl(url);
    if(image){
        // we know native image dimensions so we can optimize
        var bodyWidth = vwidth - 2 * config.columnPadding(vwidth);

        // images are never more than 2 columns wide in our designs
        var maxWidth = config.columnWidth  * 2;

        var aspectRatio = image.width / image.height;
        var height = Math.min(image.height, parseInt(bodyWidth / aspectRatio));
        var width = parseInt(height * aspectRatio);

        // if this is mobile then we'll fix the size based on its viewport
        if(vwidth <= config.mobile) figAttr = ' class="resizable" style="height: ' + height + 'px; width: ' + width + 'px"';

        // downsize oversized images
        if(width > maxWidth){
            width = maxWidth;
            height = parseInt(width / aspectRatio);
        }
        //re-route to custom size url to optimize filesize and avoid client size image resampling
        url = url.replace('/image/', '/img/') + '/' + width + '/' + height;
    }

    url = escapeCharacters(url, "*_");

    var result = "\n<figure"+ figAttr +"><div class=img-container><img src=\"" + url + "\" alt=\"" + alt_text + "\"";

    credits = (credits) ? " <span class=credit>"
        + credits
        + "</span>" : "";

    caption = caption + credits === '' ? '' : "<div class=info>i</div><figcaption>"
        + caption + credits+ "<span class=close>x</span></figcaption>";

    result += " />" + caption + "</div></figure>";

    return result;
}



function imagePreRender(text) {
    //
    // Turn Markdown image shortcuts into <img> tags.
    //

    //
    // First, handle reference-style labeled images:
    //
    // ![alt text][id]
    // caption
    //

    /*
     text = text.replace(/
     (                   // wrap whole match in $1
     !\[
     (.*?)           // alt text = $2
     \]

     [ ]?            // one optional space
     (?:\n[ ]*)?     // one optional newline followed by spaces

     \[
     (.*?)           // id = $3
     \]
     )
     ()()()()            // pad rest of backreferences
     /g, writeImageTag);
     */
    text = text.replace(/(!\[(.*?)\][ ]?(?:\n[ ]*)?\[(.*?)\])()()()()/g, writeImageTag);

    //
    // Next, handle inline images:
    //
    // ![alt text](url "optional title")
    // caption
    //
    // Don't forget: encode * and _

    /*
     text = text.replace(/
     (                   // wrap whole match in $1
     !\[
     (.*?)           // alt text = $2
     \]
     \s?             // One optional whitespace character
     \(              // literal paren
     [ \t]*
     ()              // no id, so leave $3 empty
     <?(\S+?)>?      // src url = $4
     [ \t]*
     (               // $5
     (['"])      // quote char = $6
     (.*?)       // title = $7
     \6          // matching quote
     [ \t]*
     )?              // title is optional
     \)
     )
     /g, writeImageTag);
     */
    text = text.replace(/(!\[(.*?)\]\s?\([ \t]*()<?(\S+?)>?[ \t]*((['"])(.*?)\6[ \t]*)?\)\n(.*)\n\n)/g, writeImageTag);

    return text;
}

/**
 * Function to turn relative paths to images into fully qualified CDN path
 * @param text
 * @returns {*|XML|string|void}
 */
function insertImageCDN(text){

    function prependCDN(wholeMatch, m1, offset){
        return wholeMatch.replace('(image', '(' + config.imageCDN + 'image');
    }
    return text.replace(/(?:!\[.*?\])\((image\/)/g, prependCDN);

};

function getImageByUrl(url){
    //console.log('getImageByUrl(%s)',url);
    var result = images.filter(function(i){return i.url === url})
    return result.length === 1 ? result[0] : null;
};


exports.imageToFigure = function(converter, _images, _vwidth){
    images = _images;
    vwidth = _vwidth;

    converter.hooks.chain("preConversion", function (text) {
        return insertImageCDN(text);
    });
    converter.hooks.chain("preConversion", function (text) {
        return imagePreRender(text);
    });

}
