
/**
 * Set up markdown conversion
 */
var pagedown = require("pagedown"); // npm module
var converter = new pagedown.Converter();
var pagedownExtra = require("./lib/node-pagedown-extra");
var pagedownCustom = require("./markdown-custom");
var moment = require("moment");
var config = require("./config");
var globals = require('../../config/global');


pagedownExtra.Extra.init(converter);

var renderMarkdown = function (article, vwidth) {

	var thumbnailURL = '',
		thumbnailImage = '',
		heroImage = '';

	// Create fully qualified urls for all our hosted images
	article.images.forEach(function(image){
		// Create fully qualified urls for all our hosted images (urls starting with "image/")
		if(image.url.indexOf('image/') === 0){
			image.url = config.imageCDN + image.url;
		}
		// Assign the hero image url to a var
		if ( image.type === 'hero' ) {
			heroImage = image;
			
		}
		// Assign the thumbnail image url to a var
		if ( image.type === 'thumbnail' ) {
			thumbnailImage = image;
		}
	});



	// register our custom markdown
	pagedownCustom.imageToFigure(converter, article.images, vwidth);

	// TODO - can we move this markdown processing to DB / CMS?
	// convert markdown to html
	article.body = converter.makeHtml(article.body);


	// convert published date to friendly format
	article.displayDate = moment(article.releaseDate).fromNow();

	// Create a fake rank using the ID
	article.rank = article.id;

	article.heroImage = heroImage;
	article.thumbnailImage = thumbnailImage;

	var socialTeaserImg = heroImage ? heroImage : thumbnailImage;

	article.shareConfig = {"title" : article.shortHeadline, "thumbnailURL" : thumbnailImage.url, "socialTeaserImg" : socialTeaserImg.url };
	article.shareConfig = JSON.stringify( article.shareConfig );

	// add the global config options
	article.globals = globals;

	//console.log( 'article ', article );

	return article;

};


module.exports = renderMarkdown;