var express = require('express');
var globals = require('../../config/global');




var auth = function(app){
	if(globals.settings.auth){
		//console.log('THE BASIC AUTH IS ON!');
		app.use(express.basicAuth('warp9', 'makeitso'));
	}
	return app;
};


module.exports = auth;
