
var config = require('./config');



var getViewportWidth = function (req){
	if(req.cookies && req.cookies.viewport_width){
		return req.cookies.viewport_width;
	}
	return null;
};


var layoutCheck = function (req) {

	var vwidth = false;

	// if query ?c=false then cookies are off or unsupported; show the responsive layout
	if(req.query.c && req.query.c === 'false'){
		return {layout : 'responsive', vwidth : vwidth};
	}
	// if query ?js=false then javascript is off;
	// show the responsive layout for the moment
	if(req.query.js && req.query.js === 'false'){
		return {layout : 'responsive', vwidth : vwidth};
	}

	// test to see if we have a cookie with the width set
	vwidth = getViewportWidth(req);
	if(!vwidth){
		// if there are no cookies set then trigger client side routing
		return {layout : 'cookie-check', vwidth : vwidth};
	} else {
		// display appropriate version
		if(vwidth <= config.mobile) {
			return {layout : 'mobile', vwidth : vwidth};
		}
	}
	// if nothing else?
	return {layout : 'responsive', vwidth : vwidth};
};



module.exports = {
	layoutCheck : layoutCheck
}