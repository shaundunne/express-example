var config = {
//    imageCDN : 'http://localhost:8001/',
    imageCDN : '/',

    columnWidth : 320,
    mobile : 640,
    columnPadding : function(viewportWidth){
        return parseInt(viewportWidth * 0.07);
    } /* padding in px either side of column*/

};

module.exports = config;