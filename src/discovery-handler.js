// discovery-handler.js

// Routing for category and discovery pages
var globals = require('../../config/global');
var tracking = require('./tracking');
var db = require('./db-interface');
var Q = require('q');





var discoveryHandler = function(app, req, res, category, isAjax, device){

	var template = isAjax ? 'snippets/discovery' : device.layout;

	var trackingObj = {
		headline : category.title,
		category : category.text,
		displayURL : category.href,
		byline : category.text + ' editor'

	};


	// Process the rendered HTML
	var renderCallback = function(err, html){

		if(err) {
			console.error('DISCOVER AREA ERROR', err);
			res.end();
		}

		if(isAjax) {

			res.end(JSON.stringify({category: category, html: html}));
		}
		else{
			res.end(html)
		}

	};


	db.getDiscoveryArticles(category.text)
		.then(
			function(articles){
				var layoutData = {
					globals : globals,
					tracking : tracking(trackingObj),
					category : category,
					articles : articles
				};
				app.render(template, layoutData , renderCallback)
			}
		)

};





module.exports = discoveryHandler;