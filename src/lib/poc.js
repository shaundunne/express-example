var ball = function (thing) {
	return 'boing, boing, boing';
};

var ghost = function (thing) {
	return 'WoOOOOOoooooOOOOOhhhhh';
};

var chop = function (thing) {
	return 'chop chop! - ' + thing;
};

module.exports = {
	ball : ball,
	ghost : ghost,
	test : chop
};