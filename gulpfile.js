var gulp = require('gulp');
var gutil = require('gulp-util');
var browserify = require('gulp-browserify');
var stylus = require('gulp-stylus');
var refresh = require('gulp-livereload');
var lr = require('tiny-lr');
//var ftp = require('dploy');
var server = lr();

gulp.task('stylus', function(){
    gulp.src(['src/stylus/mobile.styl', 'src/stylus/responsive.styl',  'src/stylus/common.styl'])
        .pipe(stylus({
            use: ['nib'],
            compress: true
        }))
        .pipe(gulp.dest('public/css'))
        .pipe(refresh(server))
});

gulp.task('js', function(){
    gulp.src('src/client/main.js')
        .pipe(browserify())
	    .on('error', gutil.log)
	    .pipe(gulp.dest('./public/js'))
		.on('error', gutil.log)
        .pipe(refresh(server))
		.on('error', gutil.log);
});

gulp.task('livereload', function(){
    server.listen(35729, function(err){
        if(err) return console.log(err);
    });
});

gulp.task('dev', function(){

    gulp.run('livereload', 'stylus');

	gulp.watch('src/client/*.js', function(ev){
		gulp.run('js')
	});

    gulp.watch('src/stylus/**' , function(ev){
        gulp.run('stylus')
    });

});

