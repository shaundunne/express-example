/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
var global_config = require('../config/global.js');

exports.config = {
  /**
   * Array of application names.
   */
  app_name : [global_config.settings.newRelic.appName + ' WEB'],
  /**
   * Your New Relic license key.
   */
  license_key : global_config.settings.newRelic.key,
  logging : {
    /**
     * Level at which to log. 'trace' is most useful to New Relic when diagnosing
     * issues with the agent, 'info' and higher will impose the least overhead on
     * production applications.
     */
    level : 'trace'
  }
};
