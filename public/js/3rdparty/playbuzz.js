(function () {
    function i(n, t) {
        try {
            return n.parentNode == t || n.parentNode != document && i(n.parentNode, t)
        } catch (r) {
            return !1
        }
    }

    function r(n, t) {
        var u = [],
            e = new RegExp("\\b" + n + "\\b"),
            r = t.getElementsByTagName("*"),
            i, f;
        for (t = t || document.getElementsByTagName("body")[0], i = 0, f = r.length; i < f; i++) e.test(r[i].className) && u.push(r[i]);
        return u
    }
    var n, t;
    window.PlayBuzz || (window.PlayBuzz = {});
    window.PlayBuzz.jQuery = function (t, i) {
        return new n.core.init(t, i)
    };
    n = window.PlayBuzz.jQuery;
    n.core = n.prototype = {
        init: function (t, i) {
            var r, u, f;
            if (t = t || window, i = i || document, r = typeof t == "string" ? n.selector(t, i) : r = t, this.els = [], r && typeof r.length != "undefined")
                for (u = 0, f = r.length; u < f; u++) this.els.push(r[u]);
            else this.els.push(r);
            return this
        },
        get: function (n) {
            return typeof n == "undefined" ? this.els : this.els[n]
        },
        count: function () {
            return this.els.length
        },
        each: function (n) {
            for (var t = 0, i = this.els.length; t < i; t++) n.call(this, this.els[t]);
            return this
        },
        attr: function (n, t, i) {
            return this.each(function (r) {
                typeof i == "undefined" ? r[n] = t : r[i][n] = t
            }), this
        },
        css: function (n) {
            var t = this;
            return this.each(function () {
                for (var i in n) t.attr(i, n[i], "style")
            }), this
        },
        addClass: function (n) {
            return this.each(function (t) {
                t.className += " " + n
            }), this
        },
        removeClass: function (n) {
            return this.each(function (t) {
                t.className = t.className.replace(n, "")
            }), this
        },
        on: function (n, t) {
            var i = function (i) {
                window.addEventListener ? i.addEventListener(n, t, !1) : window.attachEvent && i.attachEvent("on" + n, function () {
                    t.call(i, window.event)
                })
            };
            return this.each(function (n) {
                i(n)
            }), this
        },
        ready: function (n) {
            return t.add(n), this
        },
        remove: function () {
            return this.each(function (n) {
                n.parentNode.removeChild(n)
            }), this
        },
        offset: function () {
            var i = {
                    top: 0,
                    left: 0
                },
                t = this.get(0),
                n;
            if (doc = t && t.ownerDocument, doc) return n = doc.documentElement, t.getBoundingClientRect != undefined && (i = t.getBoundingClientRect()), {
                top: i.top + (window.pageYOffset || n.scrollTop) - (n.clientTop || 0),
                left: i.left + (window.pageXOffset || n.scrollLeft) - (n.clientLeft || 0)
            }
        },
        parent: function () {
            var n = this.get(0).parentNode;
            return PlayBuzz.jQuery(n)
        },
        width: function (n) {
            var i = {
                    top: 0,
                    left: 0
                },
                t = this.get(0);
            if (doc = t && t.ownerDocument, doc) return n ? (t.style.width = n, n) : (t.getBoundingClientRect != undefined && (i = t.getBoundingClientRect()), i.width || t.offsetWidth)
        },
        height: function () {
            var t = {
                    top: 0,
                    left: 0
                },
                n = this.get(0),
                i;
            if (doc = n && n.ownerDocument, doc) return i = doc.documentElement, n.getBoundingClientRect != undefined && (t = n.getBoundingClientRect()), t.height
        }
    };
    n.selector = function (n, t) {
        for (var h = n.split(","), u, o, s, f, e = 0; e < h.length; e++) f = h[e].replace(/ /g, ""), typeof f == "string" && (o = f.substr(0, 1), s = f.substr(1), o == "#" ? (u = document.getElementById(s), u = i(u, t) ? u : null) : u = o == "." ? r(s, t) : t.getElementsByTagName(f));
        return u
    };
    n.core.init.prototype = n.core;
    t = function () {
        var n = [],
            t = !1,
            i = function () {
                t = !0;
                for (var i = 0; i < n.length; i++) n[i].call()
            };
        return this.add = function (i) {
            if (i.constructor == String) {
                var r = i;
                i = function () {
                    eval(r)
                }
            }
            t ? i.call() : n[n.length] = i
        }, window.addEventListener ? document.addEventListener("DOMContentLoaded", function () {
            i()
        }, !1) : function () {
            if (document.uniqueID || !document.expando) {
                var n = document.createElement("document:ready");
                try {
                    alert("test");
                    n.doScroll("left");
                    i()
                } catch (t) {
                    setTimeout(arguments.callee, 0)
                }
            }
        }(), this
    }();
    n.ready = t.add
})();
window.PlayBuzz || (window.PlayBuzz = {});
PlayBuzz.Partners = function () {
    function r() {
        t = PlayBuzz.jQuery("#pb_feed").get(0) || PlayBuzz.jQuery(".pb_feed").get(0);
        n = function () {
            var n = t.attributes["data-key"];
            return n ? n.value : null
        }();
        i[n] && i[n]()
    }
    var t, n, i = {
        funniest: function () {}
    };
    return {
        render: r
    }
}();
window.JSON || (window.JSON = {}, function () {
    "use strict";

    function i(n) {
        return n < 10 ? "0" + n : n
    }

    function o(n) {
        return e.lastIndex = 0, e.test(n) ? '"' + n.replace(e, function (n) {
            var t = s[n];
            return typeof t == "string" ? t : "\\u" + ("0000" + n.charCodeAt(0).toString(16)).slice(-4)
        }) + '"' : '"' + n + '"'
    }

    function u(i, f) {
        var s, l, h, a, v = n,
            c, e = f[i];
        e && typeof e == "object" && typeof e.toJSON == "function" && (e = e.toJSON(i));
        typeof t == "function" && (e = t.call(f, i, e));
        switch (typeof e) {
        case "string":
            return o(e);
        case "number":
            return isFinite(e) ? String(e) : "null";
        case "boolean":
        case "null":
            return String(e);
        case "object":
            if (!e) return "null";
            if (n += r, c = [], Object.prototype.toString.apply(e) === "[object Array]") {
                for (a = e.length, s = 0; s < a; s += 1) c[s] = u(s, e) || "null";
                return h = c.length === 0 ? "[]" : n ? "[\n" + n + c.join(",\n" + n) + "\n" + v + "]" : "[" + c.join(",") + "]", n = v, h
            }
            if (t && typeof t == "object")
                for (a = t.length, s = 0; s < a; s += 1) typeof t[s] == "string" && (l = t[s], h = u(l, e), h && c.push(o(l) + (n ? ": " : ":") + h));
            else
                for (l in e) Object.prototype.hasOwnProperty.call(e, l) && (h = u(l, e), h && c.push(o(l) + (n ? ": " : ":") + h));
            return h = c.length === 0 ? "{}" : n ? "{\n" + n + c.join(",\n" + n) + "\n" + v + "}" : "{" + c.join(",") + "}", n = v, h
        }
    }
    typeof Date.prototype.toJSON != "function" && (Date.prototype.toJSON = function () {
        return isFinite(this.valueOf()) ? this.getUTCFullYear() + "-" + i(this.getUTCMonth() + 1) + "-" + i(this.getUTCDate()) + "T" + i(this.getUTCHours()) + ":" + i(this.getUTCMinutes()) + ":" + i(this.getUTCSeconds()) + "Z" : null
    }, String.prototype.toJSON = Number.prototype.toJSON = Boolean.prototype.toJSON = function () {
        return this.valueOf()
    });
    var f = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        e = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        n, r, s = {
            "\b": "\\b",
            "\t": "\\t",
            "\n": "\\n",
            "\f": "\\f",
            "\r": "\\r",
            '"': '\\"',
            "\\": "\\\\"
        },
        t;
    typeof JSON.stringify != "function" && (JSON.stringify = function (i, f, e) {
        var o;
        if (n = "", r = "", typeof e == "number")
            for (o = 0; o < e; o += 1) r += " ";
        else typeof e == "string" && (r = e); if (t = f, f && typeof f != "function" && (typeof f != "object" || typeof f.length != "number")) throw new Error("JSON.stringify");
        return u("", {
            "": i
        })
    });
    typeof JSON.parse != "function" && (JSON.parse = function (text, reviver) {
        function walk(n, t) {
            var r, u, i = n[t];
            if (i && typeof i == "object")
                for (r in i) Object.prototype.hasOwnProperty.call(i, r) && (u = walk(i, r), u !== undefined ? i[r] = u : delete i[r]);
            return reviver.call(n, t, i)
        }
        var j;
        if (text = String(text), f.lastIndex = 0, f.test(text) && (text = text.replace(f, function (n) {
            return "\\u" + ("0000" + n.charCodeAt(0).toString(16)).slice(-4)
        })), /^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, ""))) return j = eval("(" + text + ")"), typeof reviver == "function" ? walk({
            "": j
        }, "") : j;
        throw new SyntaxError("JSON.parse");
    })
}());
window.PlayBuzz || (window.PlayBuzz = {});
try {
    console.log("")
} catch (e) {
    window.console = {
        log: function () {},
        error: function () {}
    }
}
PlayBuzz.core || (PlayBuzz.core = function () {
    function u(n, t) {
        var i, f;
        try {
            return n.getElementsByClassName(t)
        } catch (o) {
            var u = [],
                e = new RegExp("(^| )" + t + "( |$)"),
                r = n.getElementsByTagName("div");
            for (i = 0, f = r.length; i < f; i++) e.test(r[i].className) && u.push(r[i]);
            return u
        }
    }

    function i(n, t, i) {
        n.addEventListener ? n.addEventListener(t, i) : n.attachEvent("on" + t, i)
    }

    function f(t, i) {
        if (n[t])
            for (var r = 0; r < n[t].length; r++) try {
                n[t][r](i)
            } catch (u) {}
    }

    function e() {
        try {} catch (n) {}
    }
    var r = {
            init: function () {
                this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
                this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "an unknown version";
                this.OS = this.searchString(this.dataOS) || "an unknown OS"
            },
            searchString: function (n) {
                for (var i, r, t = 0; t < n.length; t++)
                    if (i = n[t].string, r = n[t].prop, this.versionSearchString = n[t].versionSearch || n[t].identity, i) {
                        if (i.indexOf(n[t].subString) != -1) return n[t].identity
                    } else if (r) return n[t].identity
            },
            searchVersion: function (n) {
                var t = n.indexOf(this.versionSearchString);
                if (t != -1) return parseFloat(n.substring(t + this.versionSearchString.length + 1))
            },
            dataBrowser: [{
                string: navigator.userAgent,
                subString: "Chrome",
                identity: "Chrome"
            }, {
                string: navigator.userAgent,
                subString: "OmniWeb",
                versionSearch: "OmniWeb/",
                identity: "OmniWeb"
            }, {
                string: navigator.vendor,
                subString: "Apple",
                identity: "Safari",
                versionSearch: "Version"
            }, {
                prop: window.opera,
                identity: "Opera",
                versionSearch: "Version"
            }, {
                string: navigator.vendor,
                subString: "iCab",
                identity: "iCab"
            }, {
                string: navigator.vendor,
                subString: "KDE",
                identity: "Konqueror"
            }, {
                string: navigator.userAgent,
                subString: "Firefox",
                identity: "Firefox"
            }, {
                string: navigator.vendor,
                subString: "Camino",
                identity: "Camino"
            }, {
                string: navigator.userAgent,
                subString: "Netscape",
                identity: "Netscape"
            }, {
                string: navigator.userAgent,
                subString: "MSIE",
                identity: "Explorer",
                versionSearch: "MSIE"
            }, {
                string: navigator.userAgent,
                subString: "Gecko",
                identity: "Mozilla",
                versionSearch: "rv"
            }, {
                string: navigator.userAgent,
                subString: "Mozilla",
                identity: "Netscape",
                versionSearch: "Mozilla"
            }],
            dataOS: [{
                string: navigator.platform,
                subString: "Win",
                identity: "Windows"
            }, {
                string: navigator.platform,
                subString: "Mac",
                identity: "Mac"
            }, {
                string: navigator.userAgent,
                subString: "iPhone",
                identity: "iPhone/iPod"
            }, {
                string: navigator.platform,
                subString: "Linux",
                identity: "Linux"
            }]
        },
        t;
    r.init();
    var n = {},
        o = document.getElementById("pb_iframe_con") != null && document.getElementById("pb_iframe_con") != undefined;
    t = {
        browser: r,
        isMobile: function () {
            return navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i) ? !0 : !1
        }(),
        getURLParam: function (n) {
            var r, u, f, t, i, e, o;
            if (n && (n = n.toLowerCase()), r = document.location.toString(), u = r.split("?"), u.length <= 1) return null;
            var c = u[0],
                s = r.substr(c.length + 1),
                h = {};
            if (s) {
                f = s.split("&");
                try {
                    for (t = 0; t < f.length; t++)
                        if (i = f[t].split("="), i.length > 1) {
                            if (e = i[0], o = i[1].toLowerCase(), n == e.toLowerCase()) return o;
                            h[e] = o
                        }
                } catch (l) {}
            } else return null;
            return n == undefined ? h : null
        },
        getDivsByClassName: u,
        dispatchEvent: f,
        listen: function (t, i) {
            n[t] || (n[t] = []);
            n[t].push(i)
        },
        ajax: function (n, t, i, r) {
            var u = function () {
                    var n = !1;
                    if (typeof ActiveXObject != "undefined") try {
                            n = new ActiveXObject("Msxml2.XMLHTTP")
                        } catch (t) {
                            try {
                                n = new ActiveXObject("Microsoft.XMLHTTP")
                            } catch (i) {
                                n = !1
                            }
                        } else if (window.XMLHttpRequest) try {
                            n = new XMLHttpRequest
                        } catch (t) {
                            n = !1
                        }
                        return n
                }(),
                r, f;
            u && n && (u.overrideMimeType && u.overrideMimeType("text/xml"), r || (r = "text"), r = r.toLowerCase(), f = "uid=" + (new Date).getTime(), n += n.indexOf("?") + 1 ? "&" : "?", n += f, u.open("GET", n, !0), u.onreadystatechange = function () {
                if (u.readyState == 4)
                    if (u.status == 200) {
                        var result = "";
                        u.responseText && (result = u.responseText);
                        r.charAt(0) == "j" && (result = result.replace(/[\n\r]/g, ""), result = eval("(" + result + ")"));
                        i && i(result, t)
                    } else error && error(u.status)
            }, u.send(null))
        },
        loadDiv: function (n, t, i) {
            var r;
            if (window.XDomainRequest) {
                r = new XDomainRequest;
                r.onerror = function () {
                    e("there was an error loading data")
                };
                r.onload = function () {
                    i && i(r, t, !0)
                };
                r.open("get", n);
                r.send();
                return
            }
            if (window.XMLHttpRequest) {
                r = new XMLHttpRequest;
                r.onreadystatechange = function () {
                    i && i(r, t)
                };
                try {
                    r.open("GET", n, !0);
                    r.send(null)
                } catch (u) {
                    e(u)
                }
            } else r = new ActiveXObject("Microsoft.XMLHTTP"), r && (r.onreadystatechange = function () {
                i && i(r, t)
            }, r.open("GET", n, !0), r.send());
            return !1
        },
        render: function () {
            PlayBuzz.Partners && PlayBuzz.Partners.render();
            PlayBuzz.Feed && PlayBuzz.Feed.renderFeed();
            PlayBuzz.Widget && PlayBuzz.Widget.renderWidget();
            PlayBuzz.Hub && PlayBuzz.Hub.renderHub()
        },
        sendAnalyticsEvent: function (n) {
            PlayBuzz.core.sendMessageToParent({
                name: "analyticsEvent",
                event: n,
                id: PlayBuzz.core.getURLParam("divId")
            }, 1)
        },
        sendMessageToParent: function (n, i) {
            var r = document.getElementById("pb_iframe_com");
            if (r == null || r == undefined) try {
                typeof n != "string" && (n = JSON.stringify(n));
                parent.postMessage(n, "*")
            } catch (u) {
                console.error(u)
            } else try {
                r.sendMessage(JSON.stringify(n))
            } catch (u) {
                i == undefined && (i = 10);
                i > 0 && setTimeout(function () {
                    t.sendMessage(n, i - 1)
                }, 1e3)
            }
        },
        sendMessageToIframe: function (n) {
            try {
                typeof n != "string" && (n = JSON.stringify(n));
                var t = u(document, "pb_feed_iframe")[0];
                t.contentWindow.postMessage(n, "*")
            } catch (i) {}
        },
        onFrameMessage: function (t) {
            var u = t.data ? t.data : t,
                r, i;
            try {
                if (r = JSON.parse(u), r.name == "analyticsEvent" && n.analyticsEvent)
                    for (i = 0; i < n.analyticsEvent.length; i++) n.analyticsEvent[i](r.event)
            } catch (t) {}
            f("onMessage", u)
        }
    };
    try {
        t.browser.browser != "Explorer" || parseInt(t.browser.version) >= 10 ? PlayBuzz.jQuery(document).ready(t.render) : i(window, "load", t.render)
    } catch (s) {
        i(window, "load", t.render)
    }
    return i(window, "message", t.onFrameMessage), t
}());
window.PlayBuzz || (window.PlayBuzz = {});
PlayBuzz.Feed = function () {
    function v() {
        var t = {};
        return t[n.width] = 640, t[n.height] = "auto", t[n.tags] = "", t[n["social-url"]] = "auto", t[n.appData] = "auto", t[n.key] = "", t[n.game] = "auto", t[n.recommend] = "auto", t[n.social] = "true", t[n.useShares] = "true", t[n.useComments] = "true", t[n.comments] = "true", t[n["game-info"]] = "true", t[n["margin-top"]] = 0, t[n.specificItem] = "auto", t[n.targetPage] = "auto", t[n.singleItemMode] = "false", t[n.analyticsListener] = null, t
    }

    function y(t, r, u) {
        var f = i[u],
            o = {
                src: t,
                width: Math.min(Math.round(PlayBuzz.jQuery(r).width()), 640),
                height: f[n.height],
                tags: f[n.tags],
                game: f[n.game],
                key: f[n.key],
                social: f[n.social],
                comments: f[n.comments],
                parameters: f[n.params],
                socialURL: f[n["social-url"]],
                gameInfo: f[n["game-info"]],
                recommend: f[n.recommend],
                marginTop: f[n["margin-top"]],
                specificItem: f[n.specificItem],
                singleItemMode: f[n.singleItemMode],
                iframeWidth: PlayBuzz.jQuery(r).width(),
                targetPage: f[n.targetPage],
                useShares: f[n.useShares],
                useComments: f[n.useComments],
                analyticsListener: f[n.analyticsListener],
                divId: u
            },
            e;
        f[n.appData] != "" && f[n.appData] != null && f[n.appData] != undefined && (o.appData = f[n.appData]);
        PlayBuzz.iframeCreator.createFeedFrame(o, r);
        f[n.height] == "auto" && (e = null, setInterval(function () {
            var n = document.documentElement.scrollTop || window.pageYOffset;
            n != e && (e = n, PlayBuzz.core.sendMessageToIframe({
                event: "scroll",
                scroll: n,
                offsetTop: PlayBuzz.jQuery(".pb_feed_iframe").offset().top
            }))
        }, 250))
    }

    function h(t, r, e) {
        if (u.indexOf("fbtab") < 0) {
            y(t, r, e);
            return
        }
        var o = i[e],
            s = "src=" + t;
        s += "&width=" + o[n.width];
        s += "&height=" + o[n.height];
        s += "&tags=" + o[n.tags];
        s += "&game=" + o[n.game];
        s += "&key=" + o[n.key];
        s += "&social=" + o[n.social];
        s += "&comments=" + o[n.comments];
        s += "&parameters=" + o[n.params];
        s += "&socialURL=" + o[n["social-url"]];
        s += "&gameInfo=" + o[n["game-info"]];
        s += "&marginTop=" + o[n["margin-top"]];
        s += "&specificItem=" + o[n.specificItem];
        s += "&singleItemMode" + o[n.singleItemMode];
        s += "&iframeWidth=" + PlayBuzz.jQuery(r).width();
        s += "&targetPage=" + o[n.targetPage];
        s += "&useShares=" + o[n.useShares];
        s += "&useComments=" + o[n.useComments];
        s += "&analyticsListener=" + o[n.analyticsListener];
        s += "&divId=" + e;
        o[n.appData] != "" && o[n.appData] != null && o[n.appData] != undefined && (s += "&appData=" + o[n.appData]);
        s += "&recommend=" + o[n.recommend];
        PlayBuzz.core.loadDiv(document.location.protocol + "//" + f + ".playbuzz.com/publisher/feed?" + s, r, function (t, i, r) {
            if ((t.readyState == 4 && t.status == 200 || r) && (i.innerHTML = t.responseText, o[n.height] == "auto")) {
                var u = null;
                setInterval(function () {
                    var n = document.documentElement.scrollTop || window.pageYOffset;
                    n != u && (u = n, PlayBuzz.core.sendMessageToIframe({
                        event: "scroll",
                        scroll: n,
                        offsetTop: PlayBuzz.jQuery(a).offset().top
                    }))
                }, 250)
            }
        })
    }

    function p(t, r) {
        var e = i[r],
            o, s, h;
        for (o in n) e[n[o]] = t.attributes[n[o]] != undefined ? t.attributes[n[o]].value : e[n[o]];
        t.attributes.pbprefix != undefined && (f = t.attributes.pbprefix.value);
        isNaN(e[n.width]) && (e[n.width] = 640);
        PlayBuzz.core.isMobile && (s = PlayBuzz.jQuery(t).width(), h = Math.min(640, window.innerWidth), e[n.width] = Math.min(s, h));
        (isNaN(e[n.height]) || e[n.height] == "") && (e[n.height] = "auto");
        e[n.game] != "auto" && function () {
            var t = e[n.game].split("?").shift(),
                r, i, u, f;
            if (t.charAt(t.length - 1) == "/" && (r = t.split(""), r.pop(), t = r.join("")), i = t.split("/"), i.length < 2) {
                e[n.game] = "auto";
                return
            }
            if (u = i.pop(), f = i.pop(), u == "" || f == "") {
                e[n.game] = "auto";
                return
            }
            e[n.game] = f + "/" + u
        }();
        (e[n["social-url"]] == "auto" || e[n["social-url"]] == undefined || e[n["social-url"]] == null) && (e[n["social-url"]] = u)
    }

    function w(t, i) {
        if (t) {
            var r = t.attributes[n.tags] == undefined ? "all" : t.attributes[n.tags].value;
            h(document.location.protocol + "//" + f + ".playbuzz.com/tags/" + r, t, i)
        }
    }

    function b() {
        window.fbAsyncInit = function () {
            FB.init({
                appId: "527957123932456",
                status: !1,
                xfbml: !1
            });
            setInterval(function () {
                FB.Canvas.getPageInfo(function (n) {
                    PlayBuzz.core.sendMessageToIframe({
                        event: "scroll",
                        scroll: n.scrollTop,
                        offsetTop: n.offsetTop,
                        windowHeight: window.innerHeight || document.documentElement.clientHeight
                    })
                })
            }, 100)
        },
        function (n, t, i) {
            var r, u = n.getElementsByTagName(t)[0];
            n.getElementById(i) || (r = n.createElement(t), r.id = i, r.src = "//connect.facebook.net/en_US/all.js", u.parentNode.insertBefore(r, u))
        }(document, "script", "facebook-jssdk")
    }

    function k() {
        var e = PlayBuzz.jQuery(".pb_feed").els,
            o = PlayBuzz.jQuery("#pb_feed").get(0);
        (e.length > 0 && (t = e), o && t.push(o), t.length != 0) && function () {
            for (var o, k, d, nt, g, a, y, e = 0; e < t.length; e++) {
                PlayBuzz.jQuery(t[e]).addClass("pb_identifier_" + e);
                t[e].innerHTML = "";
                o = "div" + e;
                r[o] = t[e];
                i[o] = v();
                p(t[e], "div" + e);
                try {
                    k = i[o][n.params];
                    d = k.indexOf("isfb");
                    d >= 0 && (nt = k.substr(d + 7, 4) == "true", nt && !s && (s = !0, b()))
                } catch (it) {}
                var l = i[o][n.game],
                    tt = l != undefined && l != null && l != "auto" && l != "",
                    c = u.split("?")[1];
                if ((c == undefined || c.search("pb_url") == -1 && c.search("game") == -1) && !tt) {
                    w(t[e], o);
                    continue
                }
                g = tt ? i[o][n.game] : "";
                try {
                    for (c = c.split("&"), a = 0; a < c.length; a++) y = c[a].split("="), (y[0] == "pb_url" || y[0] == "game") && (g = y[1])
                } catch (it) {}
                h(document.location.protocol + "//" + f + ".playbuzz.com/" + g, t[e], o)
            }
        }()
    }

    function c() {
        var t = 50,
            i = Math.abs(window.scrollY - o),
            n = Math.min(t, i);
        window.scrollY > o && (n *= -1);
        scrollBy(0, n);
        Math.abs(n) == t ? setTimeout(c, 15) : e = !1
    }

    function d(n) {
        if (!window.scrollY) try {
            window.scrollTo(0, n);
            return
        } catch (t) {}(n && (o = n), e) || (e = !0, c())
    }

    function l(n, t, i) {
        var e = r[i],
            o = document.documentElement.scrollTop || window.pageYOffset,
            s = e.getElementsByTagName("iframe")[0],
            h = n == "iframe_top" ? 0 : n,
            u = PlayBuzz.jQuery(s).offset().top + h,
            f;
        document.location.toString().toLowerCase().search("'isfb':'true'") >= 0 ? FB.Canvas.scrollTo(0, u + 40) : u < o && (t ? (f = n == "iframe_top" ? u : n, window.scrollTo(0, f)) : d(u))
    }
    var e = !1,
        o, a, t = [],
        r = {},
        s = !1,
        u = document.location.toString().split("#")[0],
        f = function () {
            var e = u,
                n = e.split("?")[1],
                i, t, f, r;
            if (n == undefined || n.toLowerCase(), n && n.indexOf(!1))
                for (i = n.split("&"), t = 0; t < i.length; t++)
                    if (f = i[t].split("=")[0], r = i[t].split("=")[1], f == "pbprefix" && r != undefined) return r;
            return "www"
        }(),
        n = {
            width: "data-width",
            height: "data-height",
            tags: "data-tags",
            "social-url": "data-social-url",
            appData: "data-app-data",
            recommend: "data-recommend",
            game: "data-game",
            key: "data-key",
            params: "data-params",
            social: "data-social",
            useShares: "data-shares",
            useComments: "data-comments",
            analyticsListener: "data-analytics-callback",
            "game-info": "data-game-info",
            targetPage: "data-target-page",
            specificItem: "data-specific-item",
            singleItemMode: "data-single-item",
            "margin-top": "data-margin-top"
        },
        i = {};
    return PlayBuzz.core.listen("onMessage", function (e) {
        var data = JSON.parse(e),
            divId = data.id,
            functionName, noAnimation;
        if (divId && r[divId]) {
            if (data.name == "analyticsEvent") {
                try {
                    functionName = i[divId][n.analyticsListener];
                    functionName != "null" && functionName != null && eval(functionName)(data.event)
                } catch (e) {}
                return
            }
            if (data.resize_height && (window.FB && (r[divId].style.height = "98%", FB.Canvas.setSize({
                height: parseInt(data.resize_height) + 80
            })), r[divId].getElementsByTagName("iframe")[0].style.height = data.resize_height + "px", r[divId].getElementsByTagName("iframe")[0].style.maxHeight = data.resize_height + "px"), data.scroll_y != undefined && data.scroll_y != null) try {
                noAnimation = data.noAnimation == !0;
                l(data.scroll_y, noAnimation, divId)
            } catch (e) {}
            data.pageLoaded && l(0, !0, divId)
        }
    }), {
        renderFeed: function () {
            k()
        }
    }
}();
window.PlayBuzz || (window.PlayBuzz = {});
PlayBuzz.iframeCreator = function () {
    function n(n, t) {
        var r = document.createElement(n),
            i;
        for (i in t) r.setAttribute(i, t[i]);
        return r
    }

    function i(t) {
        var f = n("div", {
                "class": "pb_iframe_bottom"
            }),
            e = n("div", {
                "class": "powered_by_pb"
            }),
            r = n("a", {
                href: document.location.protocol + "//www.playbuzz.com",
                target: "_blank"
            }),
            o = n("span", {
                "class": "powered_by_pb_text"
            }),
            s = n("img", {
                src: document.location.protocol + "//az520697.vo.msecnd.net/content/images/logo_tiny.png",
                border: "0",
                width: "61",
                height: "25",
                "class": "pb_feed_small_logo"
            }),
            u, i;
        o.innerHTML = "Powered by";
        t.appendChild(f);
        f.appendChild(e);
        e.appendChild(r);
        r.appendChild(o);
        r.appendChild(s);
        u = ".pb_iframe_bottom .powered_by_pb { padding-top: 10px; float: left; }.pb_iframe_bottom .powered_by_pb a { text-decoration: none; border: 0; outline: none; }.pb_iframe_bottom .powered_by_pb .powered_by_pb_text { padding-right: 5px; color: #999; line-height: 25px; float: left; font-family: helvetica,arial,clean,sans-serif;font-size: 10px; font-style: italic; }.pb_feed_small_logo { width: 61px !important; height: 25px !important; margin-left: 5px; }.pb_iframe_bottom .powered_by_pb img { float: left; border: 0; outline: none; }.pb_iframe_bottom { overflow:hidden; }";
        i = document.createElement("style");
        document.getElementsByTagName("head")[0].appendChild(i);
        i.setAttribute("type", "text/css");
        i.styleSheet ? i.styleSheet.cssText = u : i.innerHTML = u
    }
    var t = {};
    return t.createFeedFrame = function (t, r) {
        var l, a, o, c, f;
        t.parentHost = document.location.host;
        t.parentUrl = encodeURIComponent(document.location.toString());
        var s = t.src + "?feed=true",
            e = t.width,
            u = t.height,
            h = "";
        e = e == "auto" ? "100%" : e + "px";
        u == "auto" ? (u = "100%", h = "no") : u = u + "px";
        l = parseInt(t.width);
        a = parseInt(t.height);
        isNaN(parseInt(t.width)) || (t.width = Math.round(parseInt(t.width)));
        isNaN(parseInt(t.height)) || (t.height = Math.round(parseInt(t.height)));
        for (o in t) s += "&" + o + "=" + t[o];
        c = {
            src: s,
            name: "pb_feed_iframe",
            "class": "pb_feed_iframe",
            frameBorder: "0",
            scrolling: h,
            height: u
        };
        f = n("iframe", c);
        r.appendChild(f);
        PlayBuzz.jQuery(f).css({
            width: "100%",
            maxWidth: "640px",
            height: u,
            border: "none",
            margin: "auto"
        });
        PlayBuzz.core.browser.browser == "Explorer" && PlayBuzz.core.browser.version <= 9 || PlayBuzz.jQuery(f).css("display", "table");
        try {
            if (t.key && t.key.toLowerCase() == "mlb" && document.location.hostname.toLowerCase().search("mlb.com") >= 0) return
        } catch (v) {}
        i(r)
    }, t.createWidgetFrame = function (t, i) {
        var r = n("iframe", {
            src: t
        });
        r.onload = function () {
            PlayBuzz.core.dispatchEvent("widgetReady", i)
        };
        PlayBuzz.jQuery(r).css({
            width: "100%",
            border: "none",
            scrolling: "no"
        });
        i.appendChild(r)
    }, t
}()