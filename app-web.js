/* * Module dependencies */
var global_config = require('../config/global.js');
if(global_config.settings.newRelic.enabled){
	require('newrelic');
}
var express = require('express');
var url = require('url');
var auth = require('./src/auth');
var port = global_config.settings.defaultWebPort;
var path = require('path');
var miscRouting = require('./src/routing-misc');
var articleRouting = require('./src/routing-articles');
var imageHandler = require('./src/image-handler');
var imageRouting = require('./src/routing-images');
var ip = "0.0.0.0";
var nib = require('nib');
var moment = require('moment');
var http = require('http');

// errors
var err404 = require('../config/404_article.js');
var err500 = require('../config/500_article.js');

var app = express();


app = auth(app);

/* Custom compile function using NIB */
function compile(str, path) {
	return stylus(str)
		.set('filename', path)
		.set('compress', true)
		.use(nib());
}


app.use(express.static(path.join(__dirname, 'public')));
app.use(express.logger('dev'));

app.use(require('stylus').middleware({
        src: path.join(__dirname, 'public'),
        compile: compile}));

app.use(express.cookieParser());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


/* Set up crude image server hosting the temp files off the DB */
app.use('/image', express.static(imageHandler.getPath()));

app = miscRouting(app);
app = articleRouting(app);
app = imageRouting(app);

// Handle 404
app.use(function(req, res) {
	res.status(404);
	res.render('error.jade', {globals : global_config, article : err404, format : 'responsive'});
});

// Handle 500
app.use(function(error, req, res, next) {
	res.status(500);
	res.render('error.jade', {globals : global_config, article : err500, format : 'responsive'});
	console.log('500 Error', new Date(), error);
});

process.on('uncaughtException', function(err, req, res) {
	// handle the error safely
	// TODO - do this better - http://shapeshed.com/uncaught-exceptions-in-node/
	console.error('##################### - UNCAUGHT EXCEPTION - ########################', err.stack);

});

app.listen(port, ip);
console.log("App starting on ", ip + ":" + port);


// don't forget to serve the favicon.ico! (Could we do something cool, with animated or frequently updated favicon?

